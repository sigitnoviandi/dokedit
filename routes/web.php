<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::group(['middleware' => 'auth.admin'], function () {
    // Dashboard    
    Route::get('user/dashboard', ['as' => 'user.dashboard.get', 'uses' => 'UserController@dashboard']);
    
    // Documents
    Route::get('document/manage', ['as' => 'document.manage.get', 'uses' => 'DocumentController@manage']);
    Route::get('document/manage/{action}', ['as' => 'document.manage_for_editor.get', 'uses' => 'DocumentController@manage_for_editor']);
    Route::get('document/add', ['as' => 'document.add.get', 'uses' => 'DocumentController@add']);
    Route::get('document/edit/{id}', ['as' => 'document.edit.get', 'uses' => 'DocumentController@edit']);
    Route::get('document/result/{id}', ['as' => 'document.result.get', 'uses' => 'DocumentController@result']);
    Route::post('document/save', ['as' => 'document.save.post', 'uses' => 'DocumentController@save']);
    Route::get('document/take/{document_id}', ['as' => 'document.take.get', 'uses' => 'DocumentController@take']);
    Route::get('document/download/{document_id}', ['as' => 'document.download.get', 'uses' => 'DocumentController@download']);
    Route::get('document/download/revision/{document_id}', ['as' => 'document.download.revision.get', 'uses' => 'DocumentController@download_revision']);
    Route::get('document/download/final/{document_id}', ['as' => 'document.download.final.get', 'uses' => 'DocumentController@download_final']);
    Route::get('document/submit_for_review/{document_id}', ['as' => 'document.submit_for_review.get', 'uses' => 'DocumentController@submit_for_review_form']);
    Route::post('document/submit_for_review', ['as' => 'document.submit_for_review.post', 'uses' => 'DocumentController@submit_for_review']);
    Route::get('document/remove/{id}', ['as' => 'document.remove.get', 'uses' => 'DocumentController@remove']);
    Route::get('document/cancel/{id}', ['as' => 'document.cancel.get', 'uses' => 'DocumentController@cancel']);
    Route::get('document/mark_as_complete/{id}', ['as' => 'document.mark_as_complete.get', 'uses' => 'DocumentController@mark_as_complete']);
    Route::post('document/editor_notes', ['as' => 'document.editor_notes.post', 'uses' => 'DocumentController@editor_notes']);
    Route::get('document/clear_filter', ['as' => 'document.filter.clear.get', 'uses' => 'DocumentController@clear_filter']);
    Route::get('document/clear_filter_for_editor', ['as' => 'document.filter.clear_for_editor.get', 'uses' => 'DocumentController@clear_filter_for_editor']);
    Route::post('document/set_filter', ['as' => 'document.filter.set.post', 'uses' => 'DocumentController@set_filter']);
    
    // Billing
    Route::get('billing/payment_confirmation/{id}', ['as' => 'billing.payment_confirmation.get', 'uses' => 'BillingController@payment_confirmation']);
    Route::post('billing/payment_confirmation/save', ['as' => 'billing.payment_confirmation.save', 'uses' => 'BillingController@payment_confirmation_save']);
    Route::post('billing/payment_confirmation/detail', ['as' => 'billing.payment_confirmation.detail.post', 'uses' => 'BillingController@payment_confirmation_detail']);
    Route::post('billing/payment/confirm', ['as' => 'billing.payment.confirm.post', 'uses' => 'BillingController@confirm_payment']);
    
    // Profile
    Route::get('profile/{user_id}', ['as' => 'user.profile.get', 'uses' => 'UserController@profile']);
    Route::post('profile/{user_id}/save', ['as' => 'user.profile.save.post', 'uses' => 'UserController@profile_save']);
    
    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});


Route::group(['middleware' => ['auth.superadmin']], function() {
    // Admin
    Route::get('admin/manage', ['as' => 'admin.manage.get', 'uses' => 'UserController@index_get']);
    Route::get('admin/manage/author', ['as' => 'admin.manage.author.get', 'uses' => 'UserController@index_author_get']);
    Route::get('admin/manage/editor', ['as' => 'admin.manage.editor.get', 'uses' => 'UserController@index_editor_get']);
    Route::get('admin/remove/{user_id}', ['as' => 'admin.remove.get', 'uses' => 'UserController@remove_get']);
    Route::get('admin/add/', ['as' => 'admin.add.get', 'uses' => 'UserController@add_get']);
    Route::get('admin/edit/{user_id}', ['as' => 'admin.edit.get', 'uses' => 'UserController@edit_get']);
    Route::post('admin/save', ['as' => 'admin.save.post', 'uses' => 'UserController@save_post']);
    Route::post('admin/activate', ['as' => 'admin.activate_user.post', 'uses' => 'UserController@activate_user_post']);
    Route::post('admin/resend_activation', ['as' => 'admin.resend_activation.post', 'uses' => 'UserController@resend_activation_post']);
    Route::post('admin/set_filter', ['as' => 'admin.filter.set.post', 'uses' => 'UserController@set_filter']);
    Route::get('admin/clear_filter', ['as' => 'admin.filter.clear.get', 'uses' => 'UserController@clear_filter']);
    Route::post('admin/check_email', ['as' => 'admin.check_email.post', 'uses' => 'UserController@check_email_post']);

});
Route::group(['middleware' => ['auth.guest']], function() {
    Route::post('register', ['as' => 'auth.register', 'uses' => 'Auth\RegisterController@register']);
    Route::get('auth/activate/{encrypted_user_id}/{encrypted_activation_token}', ['as' => 'auth.activate', 'uses' => 'Auth\AuthController@activate']);
    Route::post('auth/forgot_password', ['as' => 'auth.forgot_password', 'uses' => 'Auth\AuthController@forgot_password_post']);
    Route::post('auth/reset_password', ['as' => 'auth.reset_password', 'uses' => 'Auth\AuthController@reset_password_post']);
});
