<?php

// Home
Breadcrumbs::register('dashboard', function($breadcrumbs){
    $breadcrumbs->push(trans('adminlte_lang::message.dashboard'), route('user.dashboard.get'));
});

// Documents
Breadcrumbs::register('manage_documents', function($breadcrumbs){
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Dokumen', route('document.manage.get'));
});
Breadcrumbs::register('add_document', function($breadcrumbs){
    $breadcrumbs->parent('manage_documents');
    $breadcrumbs->push('Kirim Dokumen', route('document.add.get'));
});
Breadcrumbs::register('edit_document', function($breadcrumbs, $document_id){
    $breadcrumbs->parent('manage_documents');
    $breadcrumbs->push('Edit Dokumen', route('document.edit.get', $document_id));
});
Breadcrumbs::register('submit_document_for_review', function($breadcrumbs, $document_id){
    $breadcrumbs->parent('manage_documents');
    $breadcrumbs->push('Kirim Dokumen', route('document.submit_for_review.get', $document_id));
});

// Users
Breadcrumbs::register('manage_admin', function($breadcrumbs){
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Pengguna', route('admin.manage.get'));
});
Breadcrumbs::register('add_admin', function($breadcrumbs){
    $breadcrumbs->parent('manage_admin');
    $breadcrumbs->push('Pengguna Baru', route('admin.add.get'));
});
Breadcrumbs::register('edit_admin', function($breadcrumbs, $user_id){
    $breadcrumbs->parent('manage_admin');
    $breadcrumbs->push('Edit Pengguna', route('admin.edit.get', $user_id));
});
Breadcrumbs::register('view_user_profile', function($breadcrumbs, $user_id, $user_name){
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(trans('adminlte_lang::message.profile') . ' ' . $user_name, route('user.profile.get', $user_id));
});
Breadcrumbs::register('payment_confirmation', function($breadcrumbs, $document_id){
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Konfirmasi Pembayaran', route('billing.payment_confirmation.get', $document_id));
});