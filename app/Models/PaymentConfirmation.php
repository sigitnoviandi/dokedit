<?php

/*
 * Payment Confirmation Model
 * A database model to connect to the table payment_confirmation.
 *
 * @author      : Sigit Noviandi
 * @since       : June 2018
 * @package     : DokEdit
 * @copyright   : Diffrnt Digital
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class PaymentConfirmation extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'payment_confirmation';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = ['first_name', 'last_name', 'email', 'password', 'sex'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
//    protected $hidden = ['password', 'remember_token'];
    
    public static function search($filter = array(), $limit = 10, $hard_limit = FALSE) {

        $payment_confirmation = new PaymentConfirmation();
        /*
        if (is_array($filter) && !empty($filter)) {
            foreach($filter as $key => $value){
                $filter[$key] = trim($value);
            }
            if (isset($filter['filter_title']) && $filter['filter_title'] != '') {
                $documents = $documents->where('title', 'LIKE', '%' . $filter['filter_title'] . '%');
            }
            if (isset($filter['filter_no_of_pages']) && $filter['filter_no_of_pages'] != '') {
                $documents = $documents->where('no_of_pages', 'LIKE', '%' . $filter['filter_no_of_pages'] . '%');
            }
            if (isset($filter['filter_editor']) && $filter['filter_editor'] != '') {
                // get ids of the editors
                $editors = User::select('id')->whereRaw('first_name LIKE \'%' . $filter['filter_editor'] . '%\' OR last_name LIKE \'%' . $filter['filter_editor'] . '%\'')->where('type', 'editor')->get();
                $editor_ids = [];
                if($editors->isEmpty() == FALSE){
                    $editor_ids = $editors->pluck('id')->toArray();
                }
                $editor_ids[] = 'DEFAULT_EDITOR_ID';
                $documents = $documents->whereIn('editor_id', $editor_ids);
            }

//            if (isset($filter['filter_email']) && $filter['filter_email'] != '') {
//                $users = $users->where('email', 'like', '%' . $filter['filter_email'] . '%');
//            }
        }*/
        
        /*$user = Sentinel::getUser();
        if ($user->is_author()) { // authors can only see their own documents
            $documents = $documents->where('user_id', $user->id);
        }*/

        $total = $payment_confirmation->count();
        $payment_confirmation = $payment_confirmation->paginate($limit);

        return $payment_confirmation;
    }
    
    /**
     * Get the property that owns the photos.
     */
    public function document() {
        return $this->hasOne('App\Models\Document', 'document_id');
    }

}
