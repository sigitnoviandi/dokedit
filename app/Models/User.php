<?php

/* 
 * User Model
 * A database model to connect to the table user.
 *
 * @author      : Sigit Noviandi
 * @since       : April 2018
 * @package     : DokEdit
 * @copyright   : Diffrnt Digital
 */

namespace App\Models;
//use Illuminate\Database\Eloquent\Model;

use Cartalyst\Sentinel\Users\EloquentUser;

use Creativeorange\Gravatar\Facades\Gravatar;


class User extends EloquentUser {
    
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'email', 'username', 'password', 'mobile', 'organization', 'type', 'status'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    
    protected static $default_subskills = [
        'Ilmu Pengetahuan/Sains', 'Ilmu Sosial dan Humaniora', 'Teknologi dan Teknik Terapan', 'Sastra dan Kebudayaan', 'Ekonomi dan Bisnis', 'Agama', 'Bahasa Asing'
    ];
    
    /**
     * The validation rules used by the controllers.
     *
     * @var array
     */
    public static $rules = [ 
        'register' => [
            'first_name' => 'required|alpha|max:255',
            'last_name' => 'required|alpha|max:255',
            'email' => 'required|email|unique:users,email',
            'username' => 'required|username|unique:users,username',
            'sex' => 'required',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
        ],
        'login' => [
            'email' => 'required|email',
            'password' => 'required'
        ],
        'forgot_password' => [
            'email' => 'required|email'
        ]
    ];
    
    public static function validator_msg($what_for = 'register'){
        switch($what_for){
            case 'register' : 
                return [
                    'first_name.required' => trans('users/register.first_name_required'),
                    'first_name.alpha' => trans('users/register.first_name_alpha'),
                    'first_name.max' => trans('users/register.first_name_max'),
                    'last_name.required' => trans('users/register.last_name_required'),            
                    'last_name.alpha' => trans('users/register.last_name_alpha'),            
                    'last_name.max' => trans('users/register.last_name_max'),
                    'email.required' => trans('users/register.email_required'),
                    'email.email' => trans('users/register.email_email'),
                    'email.unique' => trans('users/register.email_unique'),
                    'sex.required' => trans('users/register.sex_required'),
                    'password.required' => trans('users/register.password_required'),
                    'password_confirmation.required' => trans('users/register.password_confirmation_required'),
                    'password.confirmed' => trans('users/register.password_confirmed')
                ];
                break;            
            case 'login' :
                return [                    
                    'email.required' => trans('users/login.email_required'),
                    'email.email' => trans('users/login.email_email'),
                    'password.required' => trans('users/login.password_required')
                ];
                break;
            case 'forgot_password' :
                return [                    
                    'email.required' => trans('users/forgot_password.email_required'),
                    'email.email' => trans('users/forgot_password.email_email')
                ];
                break;
        }
    }
    
    public function name(){
        return $this->first_name . ' ' . $this->last_name;
    }
    
    public static function search($filter = array(), $limit = 10, $hard_limit = FALSE) {

        $users = new User();
        if (is_array($filter) && !empty($filter)) {
            foreach($filter as $key => $value){
                $filter[$key] = trim($value);
            }
            if (isset($filter['filter_name']) && $filter['filter_name'] != '') {
                $users = $users->whereRaw('CONCAT(first_name, " ", last_name) LIKE "%' . $filter['filter_name'] . '%"');
            }
            if (isset($filter['filter_email']) && $filter['filter_email'] != '') {
                $users = $users->where('email', 'like', '%' . $filter['filter_email'] . '%');
            }
            if (isset($filter['filter_type']) && $filter['filter_type'] != '') {
                $users = $users->where('type', 'like', '%' . $filter['filter_type'] . '%');
            }
        }
        
        $total = $users->count();
        $users = $users->paginate($limit);

        return $users;
    }
    
    public function status_text(){
        if($this->status == 0){
            return '<span class="label label-warning">Not Activated</span>';
        } elseif($this->status == 1) {
            return '<span class="label label-success">Active</span>';
        }
    }
    
    public function last_login(){
        return date('d M Y H:i', strtotime($this->last_login));
    }
    
    public function is_editor(){
        return $this->type == 'editor';
    }
    
    public function is_author(){
        return $this->type == 'author';
    }
    
    public function is_admin(){
        return $this->type == 'admin';
    }
    
    public function about_me(){
        return nl2br($this->description);
    }
    
    public function avatar(){
        if(empty($this->avatar)){
            return Gravatar::get($this->email);
        } else {
            return asset('storage/' . AVATAR_PATH . '/' . $this->avatar);
        }
    }
    
    public static function default_subskills(){
        return self::$default_subskills;
    }
    
    public function subskill($pretify = FALSE){
        $subskills = unserialize($this->subskill);
        
        if($pretify == FALSE) {
            return $subskills;
        }
        
        $subskills_str = '';
        if(is_array($subskills)){
            foreach($subskills as $subskill){
                $subskills_str .= '<span class="label label-primary">' . $subskill . '</span> ';
            }
        }
        return $subskills_str;
    }
    
    /**
     * Get the featured listings belong to the instance
     */
    public function documents() {
        return $this->hasMany('App\Models\Document');
    }
    
}
