<?php

/*
 * Document Model
 * A database model to connect to the table documents.
 *
 * @author      : Sigit Noviandi
 * @since       : April 2018
 * @package     : DokEdit
 * @copyright   : Diffrnt Digital
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class Document extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'documents';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = ['first_name', 'last_name', 'email', 'password', 'sex'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
//    protected $hidden = ['password', 'remember_token'];
    
    public static function search($filter = array(), $limit = 10, $hard_limit = FALSE) {

        $documents = new Document();
        
        if (is_array($filter) && !empty($filter)) {
            foreach($filter as $key => $value){
                $filter[$key] = trim($value);
            }
            if (isset($filter['filter_title']) && $filter['filter_title'] != '') {
                $documents = $documents->where('title', 'LIKE', '%' . $filter['filter_title'] . '%');
            }
            if (isset($filter['filter_no_of_pages']) && $filter['filter_no_of_pages'] != '') {
                $documents = $documents->where('no_of_pages', 'LIKE', '%' . $filter['filter_no_of_pages'] . '%');
            }
            if (isset($filter['filter_status']) && $filter['filter_status'] != '') {
                $documents = $documents->where('status', $filter['filter_status']);
            }
            if (isset($filter['filter_editor']) && $filter['filter_editor'] != '') {
                // get ids of the editors
                $editors = User::select('id')->whereRaw('first_name LIKE \'%' . $filter['filter_editor'] . '%\' OR last_name LIKE \'%' . $filter['filter_editor'] . '%\'')->where('type', 'editor')->get();
                $editor_ids = [];
                if($editors->isEmpty() == FALSE){
                    $editor_ids = $editors->pluck('id')->toArray();
                }
                $editor_ids[] = 'DEFAULT_EDITOR_ID';
                $documents = $documents->whereIn('editor_id', $editor_ids);
            }

//            if (isset($filter['filter_email']) && $filter['filter_email'] != '') {
//                $users = $users->where('email', 'like', '%' . $filter['filter_email'] . '%');
//            }
        }
        
        $user = Sentinel::getUser();
        if ($user->is_author()) { // authors can only see their own documents
            $documents = $documents->where('user_id', $user->id);
        }

        $total = $documents->count();
        $documents = $documents->paginate($limit);

        return $documents;
    }
    
    public static function search_for_editor($filter = array(), $limit = 10, $hard_limit = FALSE) {

        $user = Sentinel::getUser();
        
        $documents = new Document();
        
        if (is_array($filter) && !empty($filter)) {
            foreach($filter as $key => $value){
                $filter[$key] = trim($value);
            }
            
            if($filter['filter_action'] == 'available'){
                $documents = $documents->where('status', 'in_the_basket');
                $documents = $documents->whereNull('editor_id');
            } elseif($filter['filter_action'] == 'in_progress'){
                $documents = $documents->where('status', 'in_progress');
                $documents = $documents->where('editor_id', $user->id);
            } elseif($filter['filter_action'] == 'completed'){
                $documents = $documents->whereIn('status', ['in_review', 'completed']);
                $documents = $documents->where('editor_id', $user->id);
            }
            
            if (isset($filter['filter_title']) && $filter['filter_title'] != '') {
                $documents = $documents->where('title', 'LIKE', '%' . $filter['filter_title'] . '%');
            }
            if (isset($filter['filter_no_of_pages']) && $filter['filter_no_of_pages'] != '') {
                $documents = $documents->where('no_of_pages', 'LIKE', '%' . $filter['filter_no_of_pages'] . '%');
            }
            if (isset($filter['filter_delivery_speed']) && $filter['filter_delivery_speed'] != '') {
                $documents = $documents->where('delivery_speed', $filter['filter_delivery_speed']);
            }
            if (isset($filter['filter_author']) && $filter['filter_author'] != '') {
                // get ids of the editors
                $authors = User::select('id')->whereRaw('first_name LIKE \'%' . $filter['filter_author'] . '%\' OR last_name LIKE \'%' . $filter['filter_author'] . '%\'')->where('type', 'author')->get();
                $author_ids = [];
                if($authors->isEmpty() == FALSE){
                    $author_ids = $authors->pluck('id')->toArray();
                }
                $author_ids[] = 'DEFAULT_AUTHOR_ID';
                $documents = $documents->whereIn('user_id', $author_ids);
            }
        }
        
        if ($user->skill == 'ejaan') { // authors can only see their own documents
            $documents = $documents->where('editing_type', 'ejaan');
        } elseif($user->skill == 'tatabahasa'){
            $documents = $documents->whereIn('editing_type', ['ejaan', 'tatabahasa']);
        }

        $total = $documents->count();
        $documents = $documents->paginate($limit);

        return $documents;
    }
    
    public function type(){
        if($this->type == 'text'){
            return 'Teks/Naskah';
        } else {
            return 'Grafis/Slide Presentasi';
        }
    }
    
    public function delivery_speed(){
        if($this->delivery_speed == 'regular'){
            return 'Reguler';
        } else {
            return 'Ekspres';
        }
    }
    
    public function status($with_label = TRUE){
        $status_text = '';
        $label_class = 'default';
        if($this->status == 'awaiting_payment'){
            $status_text = 'Menunggu Pembayaran';
            $label_class = 'warning';
        } else if($this->status == 'cancelled') {
            $status_text = 'Dibatalkan';
            $label_class = 'default';
        } else if($this->status == 'payment_confirmation_sent'){
            $status_text = 'Konfirmasi Pembayaran Diterima';
            $label_class = 'warning';
        } else if($this->status == 'in_the_basket'){
            $status_text = 'Menunggu Editor';
            $label_class = 'primary';
        } else if($this->status == 'in_progress'){
            $status_text = 'Sedang Diedit';
            $label_class = 'default';
        } else if($this->status == 'in_review'){
            $status_text = 'Menunggu Review';
            $label_class = 'primary';
        } else if($this->status == 'completed'){
            $status_text = 'Selesai';
            $label_class = 'success';
        }
        
        if($with_label == TRUE){
            $status_text = '<span class="label label-' . $label_class . '">' . $status_text . '</span>';
        }
        
        return $status_text;
    }
    
    public function payment_status($with_label = TRUE){
        if($this->status == 'cancelled'){
            return '<span class="label label-default">Dibatalkan</span>';
        }
        
        $status_text = '';
        $label_class = 'default';
        if($this->payment_status == 'unpaid'){
            $status_text = 'Belum Dibayar';
            $label_class = 'danger';
        } elseif($this->payment_status == 'paid') {
            $status_text = 'Sudah Dibayar';
            $label_class = 'success';
        }
        
        if($with_label == TRUE){
            $status_text = '<span class="label label-' . $label_class . '">' . $status_text . '</span>';
        }
        
        return $status_text;
    }
    
    public function cost($format = TRUE){
        $cost = 0;
        $unit_cost = 0;
        
        if ($this->type == 'text') {
            if ($this->editing_type == 'ejaan') {
                $unit_cost = 3000;
            } else if ($this->editing_type == 'tatabahasa') {
                $unit_cost = 4000;
            } else {
                $unit_cost = 5000;
            }
        } else {
            if ($this->editing_type == 'ejaan') {
                $unit_cost = 8000;
            } else if ($this->editing_type == 'tatabahasa') {
                $unit_cost = 9000;
            } else {
                $unit_cost = 10000;
            }
        }

        $cost = $unit_cost * $this->no_of_pages;

        if ($this->delivery_speed == 'express') {
            $cost = 2 * $cost;
        }
        
        $tax = 0.02 * $cost;
        
        $cost = $tax + $cost;
        
        if($format == TRUE){
            $cost = 'Rp ' . number_format($cost, 0, ',', '.');
        }
        
        return $cost;
    }
    
    public function payment_amount($format = TRUE){
        if(!empty($this->payment_confirmation)){
            $payment_amount = $this->payment_confirmation->amount;
        } else {
            $payment_amount = $this->cost(FALSE);
        }
        
        if($format == TRUE){
            $payment_amount = 'Rp ' . number_format($payment_amount, 0, ',', '.');
        }
        
        return $payment_amount;
    }
    
    /**
     * Get the editor
     */
    public function editor() {
        $editor = User::find($this->editor_id);
        
        return $editor;
    }
    
    public function editor_name(){
        $editor = $this->editor();
        if(empty($editor)){
            return '-';
        }
        
        return $editor->name();
    }
    
    public function available_since(){
        $available_since = date('j M Y', strtotime($this->available_since));
        if(empty($available_since)){
            return '-';
        }
        
        return $available_since;
    }
    
    public function edited_since(){
        $edited_since = date('j M Y', strtotime($this->edited_since));
        if(empty($edited_since)){
            return '-';
        }
        
        return $edited_since;
    }
    
    public function author_name(){
        $author = $this->user->name();
        if(empty($author)){
            return '-';
        }
        
        return $author;
    }
    
    
    /**
     * Get the confirmation belong to the document
     */
    public function payment_confirmation() {
        return $this->hasOne('App\Models\PaymentConfirmation', 'document_id');
    }
    
    /**
     * Get the user that owns the documents
     */
    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

}
