<?php

/*
 * Options Model
 * A database model to connect to the table options.
 *
 * @author      : Sigit Noviandi
 * @since       : April 2018
 * @package     : DOkEdit
 * @copyright   : Diffrnt Digital
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Options extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'options';
    
    public static function get_option($key = ''){
        if($key == ''){
            return NULL;
        }
        
        $config = Options::where('option_name', $key)->first();
        if(empty($config)){
            return NULL;
        }
        
        if($key == 'synctime'){
            $config->option_value = unserialize($config->option_value);
        }
        
        return $config->option_value;
    }
    
    public static function set_option($key = '', $value = ''){
        if($key == '' || $value == ''){
            return NULL;
        }
        
        $config = Options::where('option_name', $key)->first();
        if(empty($config)){
            $config = new Options();
            $config->option_name = $key;
            $config->option_value = $value;
        } else {
            $config->option_name = $key;
            $config->option_value = $value;
        }
        $config->save();
        
        return $config;
    }
}