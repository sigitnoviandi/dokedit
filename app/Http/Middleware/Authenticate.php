<?php

namespace App\Http\Middleware;

use Closure;
use Cartalyst\Sentinel\Sentinel;
use Illuminate\Support\Facades\URL;

class Authenticate {

    protected $sentinel;

    public function __construct(Sentinel $sentinel) {
        $this->sentinel = $sentinel;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        
        if ($this->sentinel->check() == false) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                \Cartalyst\Alerts\Laravel\Facades\Alert::error('Please login to access this page.');
		return redirect('login');
            }
        }
        
        ## share user profile
        $logged_user = $this->sentinel->getUser();
        $logged_user_roles = $logged_user->roles()->get();
        $roles = array();
        foreach($logged_user_roles as $role){
            $roles[] = $role->name;
        }
        
        $logged_user->role = implode(' - ', $roles);
        view()->share('logged_user', $logged_user);
        return $next($request);
    }
}
