<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

use App\Models\Options;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function __construct() {
        $support_email = Options::get_option('dokedit_support_email');
        define('DE_SUPPORT_EMAIL', $support_email);
    }
    
    public function build_breadcrumbs(){
        $dashboard = '';
        $dashboard .= '<li><a href="' . URL::Route('user.dashboard.get') . '"><i class="fa fa-dashboard"></i> </a></li>';

        $total_segments = count(Request::segments());
        for($i = 0; $i <= $total_segments; $i++){
            if($i == $total_segments - 1){
                $dashboard .= '<li>' . Request::segment($i) . '</li>';
            } else {
                $dashboard .= '<li><a>' . Request::segment($i) . '</a></li>';
            }
        }
        
        return $dashboard;
    }
    
    public function check_access($what){
        $user = Sentinel::getUser();
        if($user->inRole('super-admin')){
            return TRUE;
        }
        
        if(empty($user)){
            abort(403);
        }
        
        if(!$user->hasAccess($what)){
            abort(403);
        }
    }
}
