<?php

/*
 * User Controller
 * A controller to manage users
 *
 * @author      : Sigit Noviandi
 * @since       : April 2018
 * @package     : DokEdit
 * @copyright   : Diffrnt Digital
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Cartalyst\Alerts\Laravel\Facades\Alert;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Models\User;
use App\Models\QueueJob;
use App\Models\SyncLog;
use App\Models\Document;

class UserController extends Controller {

    protected $session_filter = 'user_filter';
    
    public function __construct() {
        parent::__construct();
    }
    
    public function index_get(Request $request){
        $limit = 50;
        
        ## check the previous request to see if we should apply the filter
        $previous_url = URL::previous();
        if($previous_url != '' && !preg_match('/admin\/manage/i', $previous_url)){
            $request->session()->forget($this->session_filter);
        }
        
        $filter = $request->session()->get($this->session_filter);
        if(isset($filter['filter_limit']) && $filter['filter_limit'] != '' && $filter['filter_limit'] > 0){
            $limit = $filter['filter_limit'];
        } else {
            $filter['filter_limit'] = $limit;
        }
        $users = User::search($filter, $limit);
        
        if($users->currentPage() == 1){
            $first_record_no = 1;
        } else {
            $first_record_no = (($users->currentPage() - 1) * $limit) + 1;
        }
        
        return view('/user/manage', ['users' => $users, 'first_record_no' => $first_record_no, 'row_no' => $first_record_no, 'filter' => $filter]);
    }
    
    public function index_author_get(Request $request){
        $limit = 50;
        
        ## check the previous request to see if we should apply the filter
        $previous_url = URL::previous();
        if($previous_url != '' && !preg_match('/admin\/manage\/author/i', $previous_url)){
            $request->session()->forget($this->session_filter);
        }
        
        $filter = $request->session()->get($this->session_filter);
        if(isset($filter['filter_limit']) && $filter['filter_limit'] != '' && $filter['filter_limit'] > 0){
            $limit = $filter['filter_limit'];
        } else {
            $filter['filter_limit'] = $limit;
        }
        $filter['filter_type'] = 'author';
        $users = User::search($filter, $limit);
        
        if($users->currentPage() == 1){
            $first_record_no = 1;
        } else {
            $first_record_no = (($users->currentPage() - 1) * $limit) + 1;
        }
        
        return view('/user/manage', ['users' => $users, 'first_record_no' => $first_record_no, 'row_no' => $first_record_no, 'filter' => $filter]);
    }
    
    public function index_editor_get(Request $request){
        $limit = 50;
        
        ## check the previous request to see if we should apply the filter
        $previous_url = URL::previous();
        if($previous_url != '' && !preg_match('/admin\/manage\/editor/i', $previous_url)){
            $request->session()->forget($this->session_filter);
        }
        
        $filter = $request->session()->get($this->session_filter);
        if(isset($filter['filter_limit']) && $filter['filter_limit'] != '' && $filter['filter_limit'] > 0){
            $limit = $filter['filter_limit'];
        } else {
            $filter['filter_limit'] = $limit;
        }
        $filter['filter_type'] = 'editor';
        $users = User::search($filter, $limit);
        
        if($users->currentPage() == 1){
            $first_record_no = 1;
        } else {
            $first_record_no = (($users->currentPage() - 1) * $limit) + 1;
        }
        
        return view('/user/manage', ['users' => $users, 'first_record_no' => $first_record_no, 'row_no' => $first_record_no, 'filter' => $filter]);
    }
    
    public function add_get(){
        $password = str_random(20);
        $role = Sentinel::getRoleRepository()->createModel();
        $roles = $role->orderBy('name')->get();

        return view('/user/add', ['password' => $password, 'roles' => $roles])->withEncryptedCsrfToken(Crypt::encrypt(csrf_token()));
    }
    
    public function edit_get($user_id){
        $user = User::find($user_id);
        $role = Sentinel::getRoleRepository()->createModel();
        $roles = $role->orderBy('name')->get();
        
        return view('/user/edit', ['user' => $user, 'roles' => $roles])->withEncryptedCsrfToken(Crypt::encrypt(csrf_token()));
    }
    
    public function check_email_post(Request $request){
        $email = $request->input('email');
        $user_id = $request->input('user_id');
        if(empty($email)){
            return response()->json(['status' => 404, 'message' => 'No email specified.']);
        }
        
        if(!empty($user_id)){
            $check = User::where('id', '<>', $user_id)->where('email', $email)->first();
        } else {
            $check = User::where('email', $email)->first();
        }
        if(!empty($check)){
            return response()->json(['status' => 404, 'message' => 'Email address is already assigned to admin: ' . $check->first_name . ' ' . $check->last_name]);
        } else {
            return response()->json(['status' => 200]);
        }
    }
    
    public function save_post(Request $request){
        $input = $request->all();
        
        if(isset($input['user_id']) && !empty($input['user_id'])){
            // edit player
            $user = Sentinel::findById($input['user_id']);
            if(empty($user)){
                Alert::error('No admin found.');
                return redirect(URL::Route('admin.manage.get'));
            }
            
            $user->first_name = $input['first_name'];
            $user->last_name = $input['last_name'];
            $user->email = $input['email'];
            
            $roles = array($input['role']);
            $user->roles()->sync($roles);
            
            if(isset($input['password']) && $input['password'] != ''){
                $user->password = password_hash($input['password'], PASSWORD_DEFAULT);
            }
            
            $result = $user->save();
            
            if($result){
                Alert::foo('The admin information has been saved.');                
            } else {
                Alert::error('We could not update the admin information at the moment. Please try again later.');  
            }
            return redirect(URL::route('admin.edit.get', [$input['user_id']]));
        } else {
            $user = Sentinel::register([
                'first_name' => $input['first_name'],
                'last_name' => $input['last_name'],
                'email' => $input['email'],
                'password' => $input['password']
            ]);
            
            ## asign a role to the user
            $role = Sentinel::findRoleById($input['role']);
            $role->users()->attach($user);
            
            ## create an activation record and send an activation email
            $activation = Activation::create($user);
            Activation::complete($user, $activation->code);
        
            if($user){
                Alert::foo('The admin information has been saved.');
                return redirect(URL::route('admin.manage.get'));
            } else {
                Alert::error('We could not save the admin information at the moment. Please try again later.'); 
                return redirect(URL::route('admin.add.get')); 
            }
        }
    }
    
    public function remove_get($user_id){
        $user = User::find($user_id);
        if(empty($user)){
            Alert::error('The admin information does not exist.');
            return redirect(URL::Route('admin.manage.get'));
        }
        $user_name = $user->first_name . ' ' . $user->last_name;
        
        $result = $user->delete();
        
        if($result){
            Alert::foo($user_name . ' has been removed.');
            return redirect(URL::Route('admin.manage.get'));
        }
        
        Alert::error('We cannot remove the admin information at this moment. Please try again later.');
        return redirect(URL::Route('admin.manage.get'));
    }
    
    public function activate_user_post(Request $request){
        $user_id = $request->post('user_id');
        
        if(empty($user_id)){
            return response()->json(['success' => FALSE, 'err_msg' => 'User ID not specified.']); 
        }
        
        $user = User::find($user_id);
        if(empty($user)){
            return response()->json(['success' => FALSE, 'err_msg' => 'User could not be found.']); 
        }
        
        $user->status = 1;
        $result = $user->save();
        
        if($result){
            return response()->json(['success' => TRUE]); 
        }
        
        return response()->json(['success' => FALSE, 'err_msg' => 'Something wrong happened when trying to activate the user.']); 
    }
    
    public function resend_activation_post(Request $request){
        $user_id = $request->post('user_id');
        
        if(empty($user_id)){
            return response()->json(['success' => FALSE, 'err_msg' => 'User ID not specified.']); 
        }
        
        $user = User::find($user_id);
        if(empty($user)){
            return response()->json(['success' => FALSE, 'err_msg' => 'User could not be found.']); 
        }
        
        ## create an activation record and send an activation email
        Activation::remove($user);
        
        $activation = Activation::createModel();
        $activation->where('user_id', $user->id)->delete();

        $sent = FALSE;
        $activation = Activation::create($user);
        if (!empty($activation)) {
            $id = Crypt::encrypt($user->id);
            $activation_token = Crypt::encrypt($activation->code);
            $data_email = array(
                'activation_link' => url('auth/activate/' . $id . '/' . $activation_token),
                'email' => $user->email,
                'name' => $user->first_name . ' ' . $user->last_name,
                'user_type' => $user->type
            );

            try {
                $sent = Mail::send('emails.users.activation', $data_email, function($message) use ($data_email) {
                            $message->from(trans('app.email_noreply'), trans('app.app_name'));
                            $message->to($data_email['email'], $data_email['name'])->subject(trans('users/register.activation_email_subject', ['app_name' => trans('app.app_name')]));
                        });
                        
                return response()->json(['success' => TRUE, 'msg' => 'Activation email has been sent to the user.']); 
            } catch (Exception $ex) {
                return response()->json(['success' => FALSE, 'err_msg' => 'Something wrong happened when trying to resend the activation email to the user. Exception msg: ' . $ex->getMessage()]); 
            }
            
        } else {
            return response()->json(['success' => FALSE, 'err_msg' => 'Something wrong happened when trying to resend the activation email to the user.']); 
        }
    }
    
    public function profile($user_id, Request $request){
        
        if($user_id == 'my'){
            $this->check_access(['user.profile.my']);
            $user = Sentinel::getUser();
        } else {
            $this->check_access(['user.profile.other']);
            $user = Sentinel::findById($user_id);
        }
        
        if(empty($user)){
            return abort(404);
        }
        
        return view('/user/profile', ['user' => $user, 'user_id' => $user_id])->withEncryptedCsrfToken(Crypt::encrypt(csrf_token()));
    }
    
    /**
     * Get a validator for an incoming user profile save request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function user_profile_save_validator(array $data, $current_user) {
        $validator = Validator::make($data, [
            'first_name' => 'required|max:191',
            'last_name' => 'required|max:191',
            'email' => 'required|email|max:191',
            'mobile' => 'required|digits_between:10,14',
            'organization' => 'max:155',
            'avatar' => 'mimes:jpeg,jpg,png|max:2048',
        ]);

        $validator->sometimes(['email'], 'unique:users', function ($input) use($current_user) {
            return (isset($input->email) && !empty($input->email) && $input->email != '' && $input->email != $current_user->email);
        });

        $validator->sometimes(['password'], 'min:6|confirmed', function ($input) {
            return (isset($input->password) && !empty($input->password) && $input->password != '');
        });
        
        $validator->sometimes(['subskill'], 'required', function ($input) {
            return ($input->type == 'editor' && $input->skill == 'substansi');
        });

        $validator->sometimes(['terms_agree_editor'], 'required', function ($input) {
            return $input->type == 'editor';
        });

        $validator->sometimes(['terms_agree'], 'required', function ($input) {
            return $input->type == 'author';
        });
        
        return $validator;
    }
    
    public function profile_save($user_id, Request $request){
        
        if($user_id == 'my'){
            $this->check_access(['user.profile.my']);
            $user = Sentinel::getUser();
            $user = User::find($user->id);
        } else {
            $this->check_access(['user.profile.other']);
            $user = User::find($user_id);
        }
        
        if(empty($user)){
            return abort(404);
        }
        
        $validator = $this->user_profile_save_validator($request->all(), $user)->validate();
        
        $data = $request->post();
        
        $user->email = $data['email'];
        
        if(!empty($data['password'])){
            $user->password = password_hash($data['password'], PASSWORD_DEFAULT);
        }
        
        if ($request->hasFile('avatar')) {
            if ($request->file('avatar')->isValid()) {
            
            } else {
                Alert::error('File yang Anda unggah tidak valid.');
                return redirect(URL::Route('user.profile.get', [$user_id]));
            }

            $file_title = $request->avatar->getClientOriginalName();
            $extension = $request->avatar->extension();

            $filename = md5($user->id) . '.' . $extension;
            $path = $request->avatar->storeAs('public/' . AVATAR_PATH, $filename);

            $filename = basename($path);

            if (empty($filename)) {
                Alert::error('Gagal mengunggah foto Anda. Mohon coba lagi dalam beberapa saat.');
                return redirect(URL::Route('user.profile.get', [$user_id]));
            }
            
            ## resize image
            $img = Image::make(Storage::path($path));
            $img->fit(500, 500, function ($constraint) {
                $constraint->upsize();
            });

            $img->save(Storage::path($path));
            
            $user->avatar = $filename;
        } 

        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->mobile = $data['mobile'];
        $user->organization = $data['organization'];
        $user->description = $data['description'];
        
        if($user->type == 'editor'){
            $user->skill = $data['skill'];
            
            if ($data['skill'] == 'substansi') {
                $subskills = $data['subskill'];

                if (!empty($data['other_subskill'])) {
                    $other_subskills = array_map('trim', $data['other_subskill']);
                    $subskills = array_merge($subskills, $other_subskills);
                }

                $user->subskill = serialize($subskills);
            }
        }
        
        $user->save();
        
        Alert::foo('Data pengguna telah disimpan.');

        return redirect(URL::Route('user.profile.get', [$user_id]));
    }

    /*public function create_default(Request $request) {

        $user = Sentinel::register([
            'first_name' => 'Sigit',
            'last_name' => 'Noviandi',
            'email' => 'sigit@diffrntdigital.com',
            'password' => 'tiger12',
            'sex' => 'Male'
        ]);

        ## asign a role to the user
        $role = Sentinel::findRoleByName('Administrator');
        $role->users()->attach($user);

        ## create an activation record and send an activation email
        $activation = Activation::create($user);
        Activation::complete($user, $activation->code);
        
        return json_encode(array('success' => TRUE, 'admin' => $user));
    }*/

    public function dashboard() {
        
        $quick_stats = $this->get_quick_stats();
        
        $data = [
            'quick_stats' => $quick_stats
        ];
        
        return view('user/dashboard', $data);
    }
    
    public function get_quick_stats(){
        $user = Sentinel::getUser();
        if($user->is_admin() == FALSE){
            return [];
        }
        
        $total_texts = Document::where('type', 'text')->count();
        $total_graphics = Document::where('type', 'graphic')->count();
        
        $total_documents = $total_texts + $total_graphics;
        $total_texts_percentage = ($total_texts / $total_documents) * 100;
        $total_graphics_percentage = ($total_graphics / $total_documents) * 100;
        
        $total_authors = User::where('type', 'author')->count();
        $total_editors = User::where('type', 'editor')->count();
        
        $total_users = $total_authors + $total_editors;
        $total_authors_percentage = ($total_authors / $total_users) * 100;
        $total_editors_percentage = ($total_editors / $total_users) * 100;
        
        $total_documents_uploaded_today = Document::where('created_at', 'LIKE', '%' . date('Y-m-d') . '%')->count();
        $total_documents_completed_today = Document::where('status', 'completed')->where('completed_at', 'LIKE', '%' . date('Y-m-d') . '%')->count();
        
        $quick_stats = [
            'total_documents' => $total_documents,
            'total_texts' => $total_texts,
            'total_texts_percentage' => $total_texts_percentage,
            'total_graphics' => $total_graphics,
            'total_graphics_percentage' => $total_graphics_percentage,
            'total_users' => $total_users,
            'total_authors' => $total_authors,
            'total_authors_percentage' => $total_authors_percentage,
            'total_editors' => $total_editors,
            'total_editors_percentage' => $total_editors_percentage,
            'total_documents_uploaded_today' => $total_documents_uploaded_today,
            'total_documents_completed_today' => $total_documents_completed_today
        ];
        
        return $quick_stats;
    }
    
    public function set_filter(Request $request){
        $input = $request->all();
        unset($input['_token']);
        $request->session()->put($this->session_filter, $input);
        
        if(isset($input['filter_type']) && $input['filter_type'] != ''){
            return redirect(URL::Route('admin.manage.' . $input['filter_type'] . '.get'));
        } else {
            return redirect(URL::Route('admin.manage.get'));
        }
    }
    
    public function clear_filter(Request $request){
        $filter = $request->session()->get($this->session_filter);
        
        $request->session()->forget($this->session_filter);
        
        if(isset($filter['filter_type']) && $filter['filter_type'] != ''){
            return redirect(URL::Route('admin.manage.' . $filter['filter_type'] . '.get'));
        } else {
            return redirect(URL::Route('admin.manage.get'));
        }
        
    }

}
