<?php

namespace App\Http\Controllers\Auth;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use App\Models\User;
use Cartalyst\Alerts\Laravel\Facades\Alert;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Laravel\Facades\Reminder;

class AuthController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Registration & Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users, as well as the
      | authentication of existing users.
      |
     */

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirect_to = NULL;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth.guest', ['except' => 'logout']);
        $this->redirect_to = URL::route('user.dashboard.get');
    }
       
    public function activate($encrypted_user_id = null, $encrypted_activation_token = null) {
        $user_id = Crypt::decrypt($encrypted_user_id);
        $activation_code = Crypt::decrypt($encrypted_activation_token);

        if (empty($user_id) || !is_numeric($user_id)) {
            Alert::error(trans('users/register.activation_invalid_url', ['support_email_link' => '<a href="mailto:' . $support_email . '">' . $support_email . '</a>']));
            return redirect(URL::route('auth.register'));
        }

        if (empty($activation_code)) {
            Alert::error(trans('users/register.activation_invalid_url', ['support_email_link' => '<a href="mailto:' . $support_email . '">' . $support_email . '</a>']));
            return redirect(URL::route('auth.register'));
        }

        $user = Sentinel::findById($user_id);

        if (empty($user)) {
            Alert::error(trans('users/register.activation_user_id_not_found'));
            return redirect(URL::route('auth.register'));
        }
        
        if ($user->status == 1) {
            Alert::error(trans('users/register.activation_user_is_activated'));
            return redirect(url('/login'));
        }

        ## TODO: CHECK IF ACTIVATION EXISTS
        if (Activation::complete($user, $activation_code)) {
            $user->status = 1;
            $user->save();
            
            ## send a welcome email
            $data_email = array(
                'login_link' => url('/login'),
                'email' => $user->email,
                'name' => $user->first_name . ' ' . $user->last_name,
                'user_type' => $user->type,
                'username' => $user->username
            );
            $sent = Mail::send('emails.users.welcome', $data_email, function($message) use ($data_email) {
                        $message->from(trans('app.email_noreply'), trans('app.app_name'));
                        $message->to($data_email['email'], $data_email['name'])->subject(trans('users/register.welcome_email_subject', ['app_name' => trans('app.app_name')]));
                    });

            Alert::foo(trans('users/register.activation_successful', ['app_name' => trans('app.app_name')]));
            return redirect(url('/login'));
        } else {
            Alert::error(trans('users/register.activation_unsuccessful'));
            return redirect(URL::route('auth.register'));
        }
    }

    public function forgot_password_post(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);
        
        $validator = $validator->validate();

        $credentials = [
            'login' => $request->input('email'),
        ];

        $user = Sentinel::findByCredentials($credentials);
        if (empty($user)) {
            Alert::error('Email yang Anda masukkan tidak terdaftar di DokEdit.');
            return redirect(url('password/reset'));
        }

        if (Activation::completed($user) == false) {
            Alert::error('Akun Anda belum diaktivasi. Silakan periksa email Anda untuk mengakivasi akun, atau hubungi kami untuk bantuan lebih lanjut.' );
            return redirect(url('password/reset'));
        }

        if(Reminder::exists($user)){
            $reminder = Reminder::createModel();
            $reminder->where('user_id', $user->id)->delete();
        }
        
        ## create an reminder record and send an activation email
        $reminder = \Cartalyst\Sentinel\Laravel\Facades\Reminder::create($user);
        if (empty($reminder)) {
            Alert::error('Kami tidak dapat memproses permintaan Anda saat ini. Silakan coba lagi setelah beberapa menit.');
            return redirect(url('password/reset'));
        }

        $id = Crypt::encrypt($user->id);
        $reminder_token = Crypt::encrypt($reminder->code);
        
        $data = [$user->id, $reminder->code];
        
        $reminder_token = Crypt::encrypt($data);
        
        $data_email = array(
            'reminder_link' => url('password/reset/' . $reminder_token),
            'email' => $user->email,
            'name' => $user->name()
        );

        $sent = Mail::send('emails.users.reminder', $data_email, function($message) use ($data_email) {
                    $message->from(trans('app.email_noreply'), trans('app.app_name'));
                    $message->to($data_email['email'], $data_email['name'])->subject(trans('users/forgot_password.reminder_email_subject', ['app_name' => trans('app.app_name')]));
                });

        Alert::foo(trans('users/forgot_password.reminder_notification'));
        return redirect(url('password/reset'));
    }

    public function reset_password_post(Request $request) {
        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            'user_id' => 'required|exists:users,id',
            'token' => 'required'
        ]);
//        die();
        $validator = $validator->validate();

        $credentials = [
            'login' => $request->input('email'),
        ];

        $data = $request->all();
        
        $user = Sentinel::findById($data['user_id']);
        if(empty($user)){
            Alert::error('Akun Anda tidak ditemukan.');
            return redirect(url('password/reset/' . $data['original_token']));
        }

        $password_reset_result = Reminder::complete($user, $data['token'], $data['password']);
        
        if ($password_reset_result) {
            Alert::foo('Password Anda berhasil diubah. Silakan login kembali.');
            return redirect(url('login/'));
        } else {
            Alert::error('Kami tidak dapat mengubah password Anda saat ini. Silakan coba lagi dalam beberapa saat.');
            return redirect(url('password/reset/'));
        }
    }

}
