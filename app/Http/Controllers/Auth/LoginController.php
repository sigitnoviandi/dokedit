<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Options;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Alerts\Laravel\Facades\Alert;

class LoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

    use AuthenticatesUsers {
        attemptLogin as attemptLoginAtAuthenticatesUsers;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest', ['except' => 'logout']);
        $this->redirect_to = URL::route('user.dashboard.get');
    }
    
    
    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm() {
        return view('adminlte::auth.login');
    }


    /**
     * Returns field name to use at login.
     *
     * @return string
     */
    public function username() {
        return config('auth.providers.users.field', 'email');
    }

    public function register_and_activate() {
        $credentials = [
            'email' => 'sigit@atomic55.net',
            'password' => 'tiger123',
        ];

        $user = Sentinel::registerAndActivate($credentials);
    }

    /**
     * Override Laravel's own authentication login post page
     *
     */
    public function login(Request $request) {
        
        try {
            $validator = Validator::make($request->all(), User::$rules['login'], User::validator_msg('login'));

            if ($validator->fails()) {
                return redirect(url('/login'))
                                ->withErrors($validator)
                                ->withInput();
            }

            $credential = array(
                'email' => $request->input('email'),
                'password' => $request->input('password')
            );
            $remember = (bool) $request->input('remember_me', false);

            if (Sentinel::authenticate($credential, $remember)) {
                return redirect($this->redirect_to);
            }

            Alert::error(trans('auth.failed'));
            return redirect(url('/login'));
        } catch (\Cartalyst\Sentinel\Checkpoints\NotActivatedException $e) {
            $support_email = Options::get_option('dokedit_support_email');
            Alert::error(trans('auth.not_activated', ['support_email_link' => '<a href="mailto:' . $support_email . '">' . $support_email . '</a>']));
            return redirect(url('/login'));
        } catch (\Cartalyst\Sentinel\Checkpoints\ThrottlingException $e) {
            $delay = $e->getDelay();
            Alert::error(trans('auth.throttle', ['seconds' => $delay]));
            return redirect(url('/login'));
        }

        return redirect(url('/login'))
                        ->withErrors($validator)
                        ->withInput();
    }
    
    public function logout() {
        Sentinel::logout(null, true);
        return redirect(url('/login'));
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request) {
        if ($this->username() === 'email')
            return $this->attemptLoginAtAuthenticatesUsers($request);
        if (!$this->attemptLoginAtAuthenticatesUsers($request)) {
            return $this->attempLoginUsingUsernameAsAnEmail($request);
        }
        return false;
    }

    /**
     * Attempt to log the user into application using username as an email.
     *
     * @param \Illuminate\Http\Request $request
     * @return bool
     */
    protected function attempLoginUsingUsernameAsAnEmail(Request $request) {
        return $this->guard()->attempt(
                        ['email' => $request->input('username'), 'password' => $request->input('password')], $request->has('remember'));
    }

}
