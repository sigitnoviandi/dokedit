<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Cartalyst\Alerts\Laravel\Facades\Alert;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

/**
 * Class RegisterController
 * @package %%NAMESPACE%%\Http\Controllers\Auth
 */
class RegisterController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Register Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users as well as their
      | validation and creation. By default this controller uses a trait to
      | provide this functionality without requiring any additional code.
      |
     */

//    use RegistersUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm() {
        return view('adminlte::auth.register');
    }

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        $validator = Validator::make($data, [
            'firstname' => 'required|max:191',
            'lastname' => 'required|max:191',
            'email' => 'required|email|max:191|unique:users',
            'username' => 'required|alpha_dash|max:155|unique:users',
            'password' => 'required|min:6|confirmed',
            'mobile' => 'required|digits_between:10,14',
            'organization' => 'max:155'
        ]);

        $validator->sometimes(['subskill'], 'required', function ($input) {
            return ($input->type == 'editor' && $input->skill == 'substansi');
        });

        $validator->sometimes(['terms_agree_editor'], 'required', function ($input) {
            return $input->type == 'editor';
        });

        $validator->sometimes(['terms_agree'], 'required', function ($input) {
            return $input->type == 'author';
        });
        
        return $validator;
    }

    public function register(Request $request) {
        $validator = $this->validator($request->all())->validate();

        $data = $request->post();
//
//        print_r($data);
//        die();
        
        $credentials = [
            'email' => $data['email'],
            'username' => $data['username'],
            'password' => $data['password'],
            'first_name' => $data['firstname'],
            'last_name' => $data['lastname'],
            'mobile' => $data['mobile'],
            'organization' => $data['organization'],
            'type' => $data['type'],
            'status' => 0
        ];

        $user = Sentinel::register($credentials);

        if (empty($user)) {
            Alert::error('Terjadi kesalahan saat mendaftarkan akun Anda. Mohon coba lagi dalam beberapa saat.');
            return redirect('register')
                            ->withErrors($validator)
                            ->withInput();
        }
        
        ## now save other data
        if($user->type == 'editor'){
            $user->skill = $data['skill'];
            
            if($data['skill'] == 'substansi'){
                $subskills = $data['subskill'];
                
                if(!empty($data['other_subskill'])){
                    $other_subskills = array_map('trim', $data['other_subskill']);
                    $subskills = array_merge($subskills, $other_subskills);
                }
                
                $user->subskill = serialize($subskills);
            }
        }
        
        $user->save();

        ## asign a role to the user
        $role = Sentinel::findRoleByName('User');
        $role->users()->attach($user);

        ## create an activation record and send an activation email
        $activation = \Activation::create($user);
        if (!empty($activation)) {
            $id = Crypt::encrypt($user->id);
            $activation_token = Crypt::encrypt($activation->code);
            $data_email = array(
                'activation_link' => url('auth/activate/' . $id . '/' . $activation_token),
                'email' => $user->email,
                'name' => $user->first_name . ' ' . $user->last_name,
                'user_type' => $user->type
            );
            
            $sent = Mail::send('emails.users.activation', $data_email, function($message) use ($data_email) {
                        $message->from(trans('app.email_noreply'), trans('app.app_name'));
                        $message->to($data_email['email'], $data_email['name'])->subject(trans('users/register.activation_email_subject', ['app_name' => trans('app.app_name')]));
                    });
        }

        Alert::foo(trans('users/register.activation_notification', ['app_name' => trans('app.app_name')]));
        return redirect('login');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data) {
        return User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
        ]);
    }

}
