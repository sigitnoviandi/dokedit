<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Crypt;
use Cartalyst\Alerts\Laravel\Facades\Alert; 

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Http\Response
     */
    public function showResetForm(Request $request, $token = null)
    {
        if(empty($token)){
            Alert::error('Token tidak valid.');
            return redirect('password/reset');
        }
        
        $token_data = Crypt::decrypt($token);
        $original_token = $token;
        
        if(empty($token_data) || count($token_data) < 2){
            Alert::error('Token tidak valid.');
            return redirect('password/reset');
        }
        
        if (empty($token_data[0]) || !is_numeric($token_data[0])) {
            Alert::error('Token tidak valid.');
            return redirect('password/reset');
        }

        return view('adminlte::auth.passwords.reset')->with(
            ['token' => $token_data[1], 'user_id' => $token_data[0], 'original_token' => $original_token]
        );
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
}
