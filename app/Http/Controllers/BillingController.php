<?php

/*
 * Billing Controller
 * A controller to manage billings
 *
 * @author      : Sigit Noviandi
 * @since       : June 2018
 * @package     : DokEdit
 * @copyright   : Diffrnt Digital
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Cartalyst\Alerts\Laravel\Facades\Alert;
use Illuminate\Support\Facades\Input;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use App\Models\Document;
use App\Models\User;
use App\Models\PaymentConfirmation;

use Illuminate\Support\Facades\Storage;

class BillingController extends Controller {

    protected $session_filter = 'document_filter';
    protected $user;

    public function __construct() {
        
    }

    public function payment_confirmation($document_id) {
        
        $user = Sentinel::getUser();
        
        $document = Document::find($document_id);
        
        if(empty($document)){
            Alert::error('Dokumen tidak ditemukan.');
            return redirect(URL::Route('document.manage.get'));
        }
        
        if($document->user_id != $user->id){
            abort('403');
        }
        
        return view('/billing/payment_confirmation', ['document' => $document])->withEncryptedCsrfToken(Crypt::encrypt(csrf_token()));
    }

    public function payment_confirmation_save(Request $request) {
        $post = Input::post();
        
        $user = Sentinel::getUser();
        if (empty($user)) {
            abort('403');
        }

        if (empty($post)) {
            Alert::error('Tidak ada data yang dikirim.');
            return redirect(URL::Route('document.manage.get'));
        }
        
        $document = Document::find($post['document_id']);
        
        if(empty($document)){
            Alert::error('Dokumen tidak ditemukan.');
            return redirect(URL::Route('document.manage.get'));
        }
        
        if ($request->hasFile('payment_screenshot')) {
            
        } else {
            Alert::error('Mohon mengunggah bukti transfer.');
            return redirect(URL::Route('billing.payment_confirmation.get', [$post['document_id']]));
        }

        if ($request->file('payment_screenshot')->isValid()) {
            
        } else {
            Alert::error('Bukti transfer yang Anda unggah tidak valid.');
            return redirect(URL::Route('billing.payment_confirmation.get', [$post['document_id']]));
        }

        $file_title = $request->payment_screenshot->getClientOriginalName();
        $extension = $request->payment_screenshot->extension();

        $user_folder = $user->id;
        $path = $request->payment_screenshot->store(PAYMENT_CONFIRMATION_PATH . $user_folder);

        $filename = basename($path);

        if (empty($filename)) {
            Alert::error('Gagal mengunggah bukti transfer Anda. Mohon coba lagi dalam beberapa saat.');
            return redirect(URL::Route('billing.payment_confirmation.get', [$post['document_id']]));
        }
        
        $payment_confirmation = new PaymentConfirmation();
        $payment_confirmation->document_id = $post['document_id'];
        $payment_confirmation->method = $post['method'];
        $payment_confirmation->paid_to = $post['paid_to'];
        $payment_confirmation->amount = $post['amount'];
        $payment_confirmation->account_owner_name = $post['account_owner_name'];
        $payment_confirmation->account_owner_no = $post['account_owner_no'];
        $payment_confirmation->payment_date = $post['payment_date'];
        $payment_confirmation->screenshot = $filename;
        
        $payment_confirmation->save();
        
        if(isset($payment_confirmation->id)){
            $document->status = 'payment_confirmation_sent';
            $document->save();
            
            Alert::foo('Konfirmasi pembayaran telah disimpan.');
            return redirect(URL::Route('document.manage.get'));
        } else {
            Alert::error('Gagal menyimpan konfirmasi pembayaran. Mohon coba lagi dalam beberapa saat.');
            return redirect(URL::Route('billing.payment_confirmation.get', [$post['document_id']]));
        }
    }
    
    public function payment_confirmation_detail(Request $request) {
        
        $user = Sentinel::getUser();
        
        if($user->hasAccess('billings.payment_confirmation.view') == FALSE){
            return response()->json(['success' => FALSE, 'err_msg' => 'You do not have access for this action.']); 
        }
        
        $document_id = $request->post('document_id');
        $document = Document::find($document_id);
        
        if(empty($document)){
            return response()->json(['success' => FALSE, 'err_msg' => 'Document could not be found.']); 
        }
        
        $confirmation = PaymentConfirmation::where('document_id', $document_id)->first();
        
        if(empty($confirmation)){
            return response()->json(['success' => FALSE, 'err_msg' => 'Payment confirmation could not be found.']); 
        }
        
        $confirmation->payment_date = date('d M Y', strtotime($confirmation->payment_date));
        $confirmation->screenshot = Storage::url('payments/file.jpg');
        
        return response()->json(['success' => TRUE, 'confirmation' => $confirmation->toArray()]); 
    }
    
    public function confirm_payment(Request $request){
        
        $user = Sentinel::getUser();
        
        if($user->hasAccess('billings.payment.confirm') == FALSE){
            return response()->json(['success' => FALSE, 'err_msg' => 'You do not have access for this action.']); 
        }
        
        $document_id = $request->post('document_id');
        
        if(empty($document_id)){
            return response()->json(['success' => FALSE, 'err_msg' => 'Document ID not specified.']); 
        }
        
        $document = Document::find($document_id);
        if(empty($document)){
            return response()->json(['success' => FALSE, 'err_msg' => 'Document could not be found.']); 
        }
        
        $document->payment_status = 'paid';
        $document->status = 'in_the_basket';
        $document->available_since = gmdate('Y-m-d H:i:s');
        $document->save();
        
        // @todo: send email to author letting them know to choose editor
        
        return response()->json(['success' => TRUE, 'msg' => 'Payment for this document has been confirmed.']); 
        
    }
}
