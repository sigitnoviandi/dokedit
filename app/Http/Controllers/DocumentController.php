<?php

/*
 * Document Controller
 * A controller to manage users
 *
 * @author      : Sigit Noviandi
 * @since       : April 2018
 * @package     : DokEdit
 * @copyright   : Diffrnt Digital
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Cartalyst\Alerts\Laravel\Facades\Alert;
use Illuminate\Support\Facades\Input;
//use Illuminate\Support\Facades\Request;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use App\Models\Document;
use App\Models\User;

use Illuminate\Support\Facades\Storage;

class DocumentController extends Controller {

    protected $session_filter = 'document_filter';
    protected $user;

    public function __construct() {
        parent::__construct();
    }

    public function manage(Request $request) {
        $this->check_access(['documents.manage']);
        
        $user = Sentinel::getUser();
        
        if($user->is_editor() == TRUE){
            abort('403');
        }
        
        $limit = 10;

        ## check the previous request to see if we should apply the filter
        $previous_url = URL::previous();
        if ($previous_url != '' && !preg_match('/document\/manage/i', $previous_url)) {
            $request->session()->forget($this->session_filter);
        }

        $filter = $request->session()->get($this->session_filter);

        $documents = Document::search($filter, $limit);

        if ($documents->currentPage() == 1) {
            $first_record_no = 1;
        } else {
            $first_record_no = (($documents->currentPage() - 1) * $limit) + 1;
        }

        return view('/document/manage', ['documents' => $documents, 'first_record_no' => $first_record_no, 'row_no' => $first_record_no, 'filter' => $filter]);
    }
    
    public function manage_for_editor(Request $request, $action = NULL) {
        $this->check_access(['documents.manage']);
        
        if(empty($action)){
            abort('403');
        }
        
        $user = Sentinel::getUser();
        
        if($user->is_editor() == FALSE){
            abort('403');
        }
        
        $limit = 10;

        ## check the previous request to see if we should apply the filter
        $previous_url = URL::previous();
        if ($previous_url != '' && !preg_match('/document\/manage\/' . $action . '/i', $previous_url)) {
            $request->session()->forget($this->session_filter);
        }

        $filter = $request->session()->get($this->session_filter);
        
        $filter['filter_action'] = $action;
        
        $documents = Document::search_for_editor($filter, $limit);

        if ($documents->currentPage() == 1) {
            $first_record_no = 1;
        } else {
            $first_record_no = (($documents->currentPage() - 1) * $limit) + 1;
        }

        return view('/document/manage_for_editor', ['documents' => $documents, 'first_record_no' => $first_record_no, 'row_no' => $first_record_no, 'filter' => $filter, 'action' => $action]);
    }

    public function add() {
        return view('/document/add', [])->withEncryptedCsrfToken(Crypt::encrypt(csrf_token()));
    }

    public function edit($id = NULL) {
        if (empty($id)) {
            Alert::error('ID dokumen yang diberikan tidak valid.');
            return redirect(URL::Route('document.manage.get'));
        }

        $document = Document::find($id);

        if (empty($document)) {
            Alert::error('Dokumen tidak ditemukan.');
            return redirect(URL::Route('document.manage.get'));
        }

        return view('/document/edit', ['document' => $document])->withEncryptedCsrfToken(Crypt::encrypt(csrf_token()));
    }

    public function save(Request $request) {
        $post = Input::post();

        $user = Sentinel::getUser();
        if (empty($user)) {
            Alert::error('Mohon login untuk melanjutkan.');
            return redirect(URL::Route('document.manage.get'));
        }

        if (empty($post)) {
            Alert::error('Tidak ada data yang dikirim.');
            return redirect(URL::Route('document.manage.get'));
        }

        if (!isset($post['action']) || $post['action'] != 'edit') {

            if ($request->hasFile('document_file')) {
                
            } else {
                Alert::error('Mohon mengunggah dokumen yang ingin Anda edit.');
                return redirect(URL::Route('document.add.get'));
            }

            if ($request->file('document_file')->isValid()) {
                
            } else {
                Alert::error('File yang Anda unggah tidak valid.');
                return redirect(URL::Route('document.add.get'));
            }

            $file_title = $request->document_file->getClientOriginalName();
            $extension = $request->document_file->extension();

            // @todo: should we do another extension checking?

            $user_folder = $user->id;
            $path = $request->document_file->store(DOCUMENTS_PATH . $user_folder);

            $filename = basename($path);

            if (empty($filename)) {
                Alert::error('Gagal mengunggah dokumen Anda. Mohon coba lagi dalam beberapa saat.');
                return redirect(URL::Route('document.add.get'));
            }

            $document = new Document();
            $document->title = $file_title;
            $document->type = $post['document_type'];
            $document->editing_type = $post['services_type'];
            $document->no_of_pages = $post['no_of_pages'];
            $document->delivery_speed = $post['services_delivery'];
            $document->user_id = $user->id;
            $document->filename = $filename;
            $document->save();

            if (!isset($document->id)) {
                Alert::error('Gagal menyimpan dokumen Anda. Mohon coba lagi dalam beberapa saat.');
                return redirect(URL::Route('document.add.get'));
            }

            Alert::foo('Dokumen Anda telah disimpan. Mohon segera melakukan pembayaran agar dokumen Anda dapat segera diproses.');
            return redirect(URL::Route('document.manage.get'));
        } else {
            $document = Document::find($post['document_id']);
            if(empty($document)){
                Alert::error('Dokumen tidak ditemukan.');
                return redirect(URL::Route('document.manage.get'));
            }
            
            if(empty($post['title'])){
                Alert::error('Judul dokumen tidak boleh kosong.');
                return redirect(URL::Route('document.manage.get'));
            }
            
            $document->title = $post['title'];
            $document->notes = $post['notes'];
            $document->save();
            
            Alert::foo('Informasi mengenai dokumen Anda telah berhasil disimpan.');
            return redirect(URL::Route('document.manage.get'));
        }
    }
    
    public function cancel($document_id){
        
        $document = Document::find($document_id);
        
        if(empty($document)){
            Alert::error('Dokumen yang ingin Anda batalkan tidak dapat ditemukan.');
            return redirect(URL::Route('document.manage.get'));
        }
        
        $user = Sentinel::getUser();
        
        if($user->is_admin() == FALSE){
            if($user->is_author() == TRUE){
                if($document->user_id != $user->id){
                    Alert::error('Anda tidak berhak untuk membatalkan proses edit dokumen ini.');
                    return redirect(URL::Route('document.manage.get'));                
                }
            } else {
                Alert::error('Anda tidak berhak untuk membatalkan proses edit dokumen ini.');
                return redirect(URL::Route('document.manage.get'));
            }
        }
        
        $document_name = $document->title;
        $document->status = 'cancelled';
        $document->save();
        
        Alert::foo($document_name . ' telah dibatalkan untuk diedit.');
        return redirect(URL::Route('document.manage.get'));
    }
    
    public function mark_as_complete($document_id){
        
        $document = Document::find($document_id);
        
        if(empty($document)){
            Alert::error('Dokumen yang ingin Anda selesaikan tidak dapat ditemukan.');
            return redirect(URL::Route('document.manage.get'));
        }
        
        $user = Sentinel::getUser();
        
        if($user->is_admin() == FALSE){
            if($user->is_author() == TRUE){
                if($document->user_id != $user->id){
                    Alert::error('Anda tidak berhak untuk menyelesaikan proses edit dokumen ini.');
                    return redirect(URL::Route('document.manage.get'));                
                }
            } else {
                Alert::error('Anda tidak berhak untuk menyelesaikan proses edit dokumen ini.');
                return redirect(URL::Route('document.manage.get'));
            }
        }
        
        $document_name = $document->title;
        $document->status = 'completed';
        $document->completed_at = gmdate('Y-m-d H:i:s');
        $document->save();
        
        Alert::foo('Proses edit dokumen ' . $document_name . ' telah selesai.');
        return redirect(URL::Route('document.manage.get'));
    }
    
    public function remove($document_id){
        
        $document = Document::find($document_id);
        
        if(empty($document)){
            Alert::error('Dokumen yang ingin Anda hapus tidak dapat kami temukan.');
            return redirect(URL::Route('document.manage.get'));
        }
        
        $user = Sentinel::getUser();
        
        if($user->is_admin() == FALSE){
            if($user->is_author() == TRUE){
                if($document->user_id != $user->id){
                    Alert::error('Anda tidak berhak untuk menghapus dokumen ini.');
                    return redirect(URL::Route('document.manage.get'));                
                }
            } else {
                Alert::error('Anda tidak berhak untuk menghapus dokumen ini.');
                return redirect(URL::Route('document.manage.get'));
            }
        }
        
        $document_name = $document->title;
        $document->delete();
        
        Alert::foo($document_name . ' telah dihapus.');
        return redirect(URL::Route('document.manage.get'));
    }
    
    public function take($document_id){
        
        $user = Sentinel::getUser();
        
        if($user->is_editor() == FALSE){
            abort('403');
        }
        
        $document = Document::find($document_id);
        
        if(empty($document)){
            Alert::error('Dokumen yang ingin Anda ambil tidak dapat kami temukan.');
            return redirect(URL::Route('document.manage_for_editor.get', ['available']));
        }
        
        if(!empty($document->editor_id) && $document->status != 'in_the_basket'){
            Alert::error('Dokumen tidak lagi tersedia untuk Anda edit.');
            return redirect(URL::Route('document.manage_for_editor.get', ['available']));
        }
        
        $document->editor_id = $user->id;
        $document->status = 'in_progress';
        $document->edited_since = gmdate('Y-m-d H:i:s');
        $document->save();
        
        Alert::foo('Dokumen telah ditambah ke daftar pekerjaan Anda.');
        return redirect(URL::Route('document.manage_for_editor.get', ['available']));
    }
    
    public function download($document_id){
        
        $user = Sentinel::getUser();
        
        if($user->is_editor() == FALSE && $user->is_admin() == FALSE){
            abort('403');
        }
        
        $document = Document::find($document_id);
        
        if(empty($document)){
            Alert::error('Dokumen tidak ditemukan.');
            return redirect(URL::Route('document.manage_for_editor.get', ['in_progress']));
        }
        
        if($document->editor_id != $user->id){
            abort('403');
        }
        
        if($document->status != 'in_progress'){
            Alert::error('Dokumen tidak lagi tersedia untuk Anda edit.');
            return redirect(URL::Route('document.manage_for_editor.get', ['in_progress']));
        }
        
        $file_path = 'documents/' . $document->user_id . '/' . $document->filename;
        $storage_file_path = Storage::path($file_path);
        
        $file_extension = \Illuminate\Support\Facades\File::extension($storage_file_path);
        $file_new_name = $document->title;
        
        if(preg_match('/\.' . $file_extension . '/', $file_new_name)){
            
        } else {
            $file_new_name = $file_new_name . '.' . $file_extension;
        }
        
        return Storage::download($file_path, $file_new_name);
    }
    
    public function download_revision($document_id){
        
        $user = Sentinel::getUser();
        
        if($user->is_author() == FALSE && $user->is_admin() == FALSE){
            abort('403');
        }
        
        $document = Document::find($document_id);
        
        if(empty($document)){
            Alert::error('Dokumen tidak ditemukan.');
            return redirect(URL::Route('document.manage.get'));
        }
        
        if($document->user_id != $user->id){
            abort('403');
        }
        
        if($document->status != 'in_review' && $document->status != 'completed'){
            Alert::error('Dokumen belum selesai diedit.');
            return redirect(URL::Route('document.manage.get'));
        }
        
        if(empty($document->filename_revision)){
            Alert::error('File catatan revisi belum diupload oleh editor.');
            return redirect(URL::Route('document.manage.get'));
        }
        
        $file_path = 'documents/' . $document->user_id . '/' . $document->filename_revision;
        $storage_file_path = Storage::path($file_path);
        
        $file_extension = \Illuminate\Support\Facades\File::extension($storage_file_path);
        $file_new_name = $document->title;
        
        if(preg_match('/\.' . $file_extension . '/', $file_new_name)){
            
        } else {
            $file_new_name = $file_new_name . '_dokedit_revision_history.' . $file_extension;
        }
        
        return Storage::download($file_path, $file_new_name);
    }
    
    public function download_final($document_id){
        
        $user = Sentinel::getUser();
        
        if($user->is_author() == FALSE && $user->is_admin() == FALSE){
            abort('403');
        }
        
        $document = Document::find($document_id);
        
        if(empty($document)){
            Alert::error('Dokumen tidak ditemukan.');
            return redirect(URL::Route('document.manage.get'));
        }
        
        if($document->user_id != $user->id){
            abort('403');
        }
        
        if($document->status != 'in_review' && $document->status != 'completed'){
            Alert::error('Dokumen belum selesai diedit.');
            return redirect(URL::Route('document.manage.get'));
        }
        
        if(empty($document->filename_final)){
            Alert::error('File final hasil revisi belum diupload oleh editor.');
            return redirect(URL::Route('document.manage.get'));
        }
        
        $file_path = 'documents/' . $document->user_id . '/' . $document->filename_final;
        $storage_file_path = Storage::path($file_path);
        
        $file_extension = \Illuminate\Support\Facades\File::extension($storage_file_path);
        $file_new_name = $document->title;
        
        if(preg_match('/\.' . $file_extension . '/', $file_new_name)){
            
        } else {
            $file_new_name = $file_new_name . '_dokedit_final.' . $file_extension;
        }
        
        return Storage::download($file_path, $file_new_name);
    }
    
    public function submit_for_review_form($document_id = NULL) {
        
        $user = Sentinel::getUser();
        
        if($user->is_editor() == FALSE){
            abort('403');
        }
        
        $document = Document::find($document_id);
        
        if(empty($document)){
            Alert::error('Dokumen tidak ditemukan.');
            return redirect(URL::Route('document.manage_for_editor.get', ['in_progress']));
        }
        
        if($document->editor_id != $user->id){
            abort('403');
        }
        
        if($document->status != 'in_progress'){
            Alert::error('Dokumen tidak lagi tersedia untuk Anda edit.');
            return redirect(URL::Route('document.manage_for_editor.get', ['in_progress']));
        }
        
        return view('/document/submit_for_review', ['document' => $document])->withEncryptedCsrfToken(Crypt::encrypt(csrf_token()));
    }
    
    public function submit_for_review(Request $request) {
        $post = Input::post();

        $user = Sentinel::getUser();
        if (empty($user)) {
            abort('403');
        }

        if (empty($post)) {
            Alert::error('Tidak ada data yang dikirim.');
            return redirect(URL::Route('document.manage.get'));
        }
        
        if($user->is_editor() == FALSE){
            abort('403');
        }
        
        $document = Document::find($post['document_id']);
        
        if(empty($document)){
            Alert::error('Dokumen tidak ditemukan.');
            return redirect(URL::Route('document.manage_for_editor.get', ['in_progress']));
        }
        
        if($document->editor_id != $user->id){
            abort('403');
        }
        
        if ($request->hasFile('filename_final')) {
            
        } else {
            Alert::error('Mohon mengunggah dokumen final yang telah Anda edit.');
            return redirect(URL::Route('document.submit_for_review.get', [$post['document_id']]));
        }

        if ($request->file('filename_final')->isValid()) {
            
        } else {
            Alert::error('File dokumen final yang Anda unggah tidak valid.');
            return redirect(URL::Route('document.submit_for_review.get', [$post['document_id']]));
        }
        
        if($document->type == 'text'){
            if ($request->hasFile('filename_revision')) {

            } else {
                Alert::error('Mohon mengunggah file dengan catatan revisi Anda.');
                return redirect(URL::Route('document.submit_for_review.get', [$post['document_id']]));
            }

            if ($request->file('filename_revision')->isValid()) {

            } else {
                Alert::error('File catatan revisi yang Anda unggah tidak valid.');
                return redirect(URL::Route('document.submit_for_review.get', [$post['document_id']]));
            }
        }

        $final_file_title = $request->filename_final->getClientOriginalName();
        $extension = $request->filename_final->extension();

        // @todo: should we do another extension checking?

        $user_folder = $document->user_id;
        $path = $request->filename_final->store(DOCUMENTS_PATH . $user_folder);

        $filename_final = basename($path);

        if (empty($filename_final)) {
            Alert::error('Gagal mengunggah file final yang telah Anda edit. Mohon coba lagi dalam beberapa saat.');
            return redirect(URL::Route('document.submit_for_review.get', [$post['document_id']]));
        }
        
        $filename_revision = '';
        if($document->type == 'text'){
            
            $revision_file_title = $request->filename_revision->getClientOriginalName();
            $extension = $request->filename_revision->extension();

            // @todo: should we do another extension checking?

            $user_folder = $document->user_id;
            $path = $request->filename_revision->store(DOCUMENTS_PATH . $user_folder);

            $filename_revision = basename($path);

            if (empty($filename_revision)) {
                // remove previously uploaded file
                Storage::delete(DOCUMENTS_PATH . $user_folder . '/' . $filename_final);

                Alert::error('Gagal mengunggah file catatan revisi Anda. Mohon coba lagi dalam beberapa saat.');
                return redirect(URL::Route('document.submit_for_review.get', [$post['document_id']]));
            }
        }

        $document->filename_final = $filename_final;
        if(!empty($filename_revision)){
            $document->filename_revision = $filename_revision;
        }
        if(isset($post['editor_notes']) && !empty($post['editor_notes'])){
            $document->editor_notes = $post['editor_notes'];
        }
        
        $document->status = 'in_review';
        $document->submitted_at = gmdate('Y-m-d H:i:s');
        $document->save();

        Alert::foo('Dokumen yang telah Anda edit telah dikirim kepada pemilik untuk diperiksa.');
        return redirect(URL::Route('document.manage_for_editor.get', ['in_progress']));
    }
    
    public function editor_notes(Request $request) {
        
        $user = Sentinel::getUser();
        
        if($user->is_author() == FALSE && $user->is_admin() == FALSE){
            return response()->json(['success' => FALSE, 'err_msg' => 'Anda tidak dapat menggunakan fitur ini.']); 
        }
        
        $document_id = $request->post('document_id');
        $document = Document::find($document_id);
        
        if(empty($document)){
            return response()->json(['success' => FALSE, 'err_msg' => 'Dokumen tidak dapat ditemukan.']); 
        }
        
        if($user->is_author() == TRUE && $document->user_id != $user->id){
            return response()->json(['success' => FALSE, 'err_msg' => 'Anda tidak dapat menggunakan fitur ini.']); 
        }
        
        return response()->json(['success' => TRUE, 'editor_notes' => nl2br($document->editor_notes)]); 
    }

    public function set_filter(Request $request) {
        $input = $request->all();
        unset($input['_token']);
        $request->session()->put($this->session_filter, $input);

        if(isset($input['action']) && $input['action'] != ''){
            return redirect(URL::Route('document.manage_for_editor.get', [$input['action']]));
        } else {
            return redirect(URL::Route('document.manage.get'));
        }
    }

    public function clear_filter(Request $request) {
        $request->session()->forget($this->session_filter);

        return redirect(URL::Route('document.manage.get'));
    }

    public function clear_filter_for_editor(Request $request, $action = 'available') {
        $request->session()->forget($this->session_filter);

        return redirect(URL::Route('document.manage_for_editor.get', [$action]));
    }

}
