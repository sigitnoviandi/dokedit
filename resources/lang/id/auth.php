<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Alamat email atau kata sandi yang Anda masukkan salah.',
    'throttle' => 'Anda melebihi batas percobaan untuk masuk/login. Mohon mencoba lagi setelah :seconds detik.',
    'not_activated' => 'Akun Anda belum diaktifkan. Silakan periksa kembali inbox email Anda untuk melakukan aktivasi melalui email yang kami kirimkan saat Anda mendaftar. Apabila Anda masih mengalami kesulitan, silakan mengirim email ke :support_email_link.'
];
