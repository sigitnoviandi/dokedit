<?php

/*
  |--------------------------------------------------------------------------
  | Reset Password Language
  |--------------------------------------------------------------------------
  |
  | The following language lines are used by the reset password modul.
  |
 */

return array(
    
    /* -----------------------------------------------------------------------
     * Page Title & Meta
     * -----------------------------------------------------------------------
     */    
    'page_title' => 'Ganti/Reset Password',
    
    /* -----------------------------------------------------------------------
     * Page Heading
     * -----------------------------------------------------------------------
     */    
    'page_heading' => 'Ganti/Reset Password',
    
    
    /* -----------------------------------------------------------------------
     * forms
     * -----------------------------------------------------------------------
     */  
    
    'password_placeholder' => 'Password/Kata Sandi Baru',
    'confirm_password_placeholder' => 'Konfirmasi Password/Kata Sandi Baru', 
    
    'submit_button_text' => 'Ubah Password/Kata Sandi',
    'submit_button_title' => 'Klik untuk Mengubah Password/Kata Sandi',
    
    'login_link_text' => 'Login/Masuk',
    'login_link_title' => 'Klik untuk Login/Masuk',
    
    'register_link_text' => 'Daftar',
    'register_link_title' => 'Klik untuk Mendaftar',
    
    
    /* -----------------------------------------------------------------------
     * notifications
     * -----------------------------------------------------------------------
     */
    'error_title' => 'Kami menemukan beberapa kesalahan:',
    'password_required' => 'Password/Kata Sandi harus diisi',
    'password_confirmation_required' => 'Konfirmasi Password/Kata Sandi harus diisi',
    'password_confirmed' => 'Konfirmasi Password tidak sama dengan Password',
    
    'user_not_found' => 'Akun user dengan alamat email <strong>:email</strong> tidak ditemukan.',
    'user_inactivated' => 'Akun Anda belum teraktivasi. Klik <a href=":resend_activation_url">di sini</a> untuk menerima kembali email aktivasi dari kami.',
    'reminder_fail' => 'Password/kata sandi Anda tidak dapat diubah saat ini. Silakan mencoba beberapa saat lagi.',
    'user_blocked' => 'Demi keamanan, Anda tidak dapat login melalui komputer ini untuk sementara. Silakan login kembali setelah :blocked_seconds_remaining detik.',
    
    'reminder_notification' => 'Kami telah mengirimkan sebuah email berisi panduan untuk mengganti password/kata sandi Anda.',
    'reminder_invalid_url' => 'URL untuk mengganti password/kata sandi Anda tidak valid. Silakan hubungi kami untuk bantuan lebih lanjut.',
    'reminder_user_id_not_found' => 'Akun Anda tidak ditemukan.',
    'reminder_not_exist' => 'Kami tidak menemukan permohonan Anda untuk mengganti password. Gunakan form di bawah ini untuk mengganti password Anda.',
    'reminder_unsuccessful' => 'Password/kata sandi Anda tidak dapat diganti saat ini. Mohon mencoba lagi dalam beberapa saat.',
    'reminder_successful' => 'Selamat! Password/kata sandi Anda telah diganti! Silakan login menggunakan form di bawah ini untuk mulai mengatur pernikahan Anda.',
    'Reset_email_subject' => 'Selamat! Password anda telah sukses diganti'
    
    
    
    /* -----------------------------------------------------------------------
     * emails
     * -----------------------------------------------------------------------
     */
    'succesfully_reset' =>'
<p>Halo :name!</p>
<p>Kami telah BERHASIL memproses permohonan untuk melakukan perubahan password / kata sandi akun:app_name Anda. </p>
<p>silahkan kembali ke halaman sebelumnya untuk melanjutkan aktivitas di aturnikah.com</p>
<p>berikut link untuk kembali ke halaman sebelumnya</p>
<p>:Goback_link</p>
<br/><br/>
<p>Salam,</p>
<br/>
<p>Tim :app_name</p>
',
    
    
    
    'reminder_email_subject' => 'Permohonan Perubahan Password/Kata Sandi - :app_name',
    'reminder_email_content' => '
<p>Halo :name!</p>

<p>Kami telah menerima permohonan untuk melakukan perubahan password/kata sandi akun :app_name Anda. </p>
<p>Jika Anda TIDAK bermaksud mengganti password/kata sandi Anda, abaikan email ini (password/kata sandi Anda tidak akan berubah).</p>
<p>Kalau Anda bermaksud mengganti password/kata sandi Anda, silakan gunakan tautan di bawah ini:</p>

<p>:reminder_link</p>

<p>Tautan ini akan berlaku/aktif selama 4 jam.</p>

<p>Apabila Anda mengalami kesulitan, jangan ragu-ragu untuk menghubungi kami. </p>
<br/><br/>
<p>Salam,</p>
<br/>
<p>Tim :app_name</p>
',
    
);
