<?php

/*
  |--------------------------------------------------------------------------
  | Dashboard Language
  |--------------------------------------------------------------------------
  |
  | The following language lines are used in the dashboard page.
  |
 */

return array(
    
    /* -----------------------------------------------------------------------
     * Page Title & Meta
     */
    
    'page_title' => 'Dashboard',
    
    /* -----------------------------------------------------------------------
     * Page Heading
     */    
    'page_heading' => 'Dashboard',
    'page_subheading' => 'Overview Umum',
    'page_breadcrumb' => 'Dasbor',
    
    /* -----------------------------------------------------------------------
     * Page Content
     */    
    'content' => 'Selamat datang di dasbor',
    
    
    
    
);
