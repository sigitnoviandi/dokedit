<?php

/*
  |--------------------------------------------------------------------------
  | User Profile Language
  |--------------------------------------------------------------------------
  |
  | The following language lines are used in the user profile page.
  |
 */

return array(
    
    /* -----------------------------------------------------------------------
     * Page Title & Meta
     */
    
    'page_title' => 'Profil :name',
    
    /* -----------------------------------------------------------------------
     * Page Heading
     */    
    'page_heading' => 'Profil :name',
    'page_subheading' => 'Informasi profil :name',
    
    /* -----------------------------------------------------------------------
     * Page Content
     */    
    'content'                       => 'Selamat datang di dasbor',
    'user_not_found'                => 'Profil pengguna tidak dapat ditemukan.',
    'edit_my_profile_link_text'     => 'Edit Profil Saya',
    'edit_profile_link_text'        => 'Edit Profil Pengguna',
    'user_type'                     => 'Tipe Akun',
    'username'                      => 'Username',
    'last_login'                      => 'Login terakhir',
    'email'                      => 'Email',
    'mobile'                      => 'No. HP',
    'organization'                      => 'Organisasi/Perusahaan',
    'skill'                      => 'Keahlian',
    'subskill'                      => 'Substansi Keahlian',
    'about_me'                      => 'Tentang Saya',
    'reviews'                      => 'Ulasan/Komentar',
    'edit'                      => 'Edit Profil Saya',
    'edit'                      => 'Edit Profil Saya',
    
    
);
