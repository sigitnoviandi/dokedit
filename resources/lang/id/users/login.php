<?php

/*
  |--------------------------------------------------------------------------
  | Login Language
  |--------------------------------------------------------------------------
  |
  | The following language lines are used by the login/logout modul.
  |
 */

return array(
    
    /* -----------------------------------------------------------------------
     * Page Title & Meta
     * -----------------------------------------------------------------------
     */    
    'page_title' => 'Login/Masuk',
    
    /* -----------------------------------------------------------------------
     * Page Heading
     * -----------------------------------------------------------------------
     */    
    'page_heading' => 'Login/Masuk',
    
    
    /* -----------------------------------------------------------------------
     * forms
     * -----------------------------------------------------------------------
     */    
    'email_placeholder' => 'Email',
    'password_placeholder' => 'Password/Kata Sandi',
    'remember_me_label' => 'Biarkan saya tetap masuk',    
    'login_button_text' => 'Masuk',
    'login_button_title' => 'Login/Masuk',
    'or_social_signup' => 'ATAU',
    'social_signin_facebook' => 'Login/Masuk dengan Akun Facebook Anda',
    'social_signin_twitter' => 'Login/Masuk dengan Akun Twitter Anda',
    
    'forgot_password_link_text' => 'Lupa password?',
    'forgot_password_link_title' => 'Klik untuk Mengganti Password Anda',
    
    'register_link_text' => 'Belum Terdaftar?',
    'register_link_title' => 'Klik untuk Mendaftar',
    
    
    /* -----------------------------------------------------------------------
     * notifications
     * -----------------------------------------------------------------------
     */
    'error_title' => 'Kami menemukan beberapa kesalahan:',
    'email_required' => 'Alamat Email harus diisi',
    'email_email' => 'Alamat Email tidak valid',
    'password_required' => 'Password/Kata Sandi harus diisi',
    'unauthorized_access' => 'Silakan login terlebih dahulu.',
    'login_fail' => 'Email/password yang Anda masukkan salah.',
    'password_empty' => 'Masukkan sandi/password Anda.',
    'user_inactivated' => 'Akun Anda belum teraktivasi. Klik <a href=":resend_activation_url">di sini</a> untuk menerima kembali email aktivasi dari kami.',
    'user_blocked' => 'Demi keamanan, Anda tidak dapat login melalui komputer ini untuk sementara. Silakan login kembali setelah :blocked_seconds_remaining detik.',
    
);
