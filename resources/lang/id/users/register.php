<?php

/*
  |--------------------------------------------------------------------------
  | Register Language
  |--------------------------------------------------------------------------
  |
  | The following language lines are used by the user register modul.
  |
 */

return array(
    
    /* -----------------------------------------------------------------------
     * Page Title & Meta
     * -----------------------------------------------------------------------
     */    
    'page_title' => 'Daftar',
    
    
    /* -----------------------------------------------------------------------
     * Page Heading
     * -----------------------------------------------------------------------
     */    
    'page_heading' => 'Daftar Menjadi Anggota',
    
    
    /* -----------------------------------------------------------------------
     * forms
     * -----------------------------------------------------------------------
     */    
    'first_name_placeholder' => 'Nama Depan',
    'last_name_placeholder' => 'Nama Belakang',
    'email_placeholder' => 'Email',
    'password_placeholder' => 'Password/Kata Sandi',
    'confirm_password_placeholder' => 'Konfirmasi Password/Kata Sandi', 
    'sex' => 'Jenis Kelamin:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
    'sex_male' => 'Pria',
    'sex_female' => 'Wanita',
    'register_button_text' => 'Daftar',
    'register_button_title' => 'Simpan Data dan Daftar Menjadi Anggota',    
    'or_social_signup' => 'ATAU',
    'social_signup_facebook' => 'Daftar dengan Akun Facebook Anda',
    'social_signup_twitter' => 'Daftar dengan Akun Twitter Anda',
    'login_link_text' => 'Sudah pernah mendaftar?',
    'login_link_title' => 'Login',
    'all_fields_are_required' => 'Semua data harus diisi.',
    
    /* -----------------------------------------------------------------------
     * notifications
     * -----------------------------------------------------------------------
     */
    'error_title' => 'Kami menemukan beberapa kesalahan:',
    'first_name_required' => 'Nama Depan harus diisi',
    'first_name_alpha' => 'Nama Depan hanya boleh diisi dengan karakter alfabetik (A-Z)',
    'first_name_max' => 'Nama Depan tidak boleh lebih dari :max karakter',
    'last_name_required' => 'Nama Belakang harus diisi',
    'last_name_alpha' => 'Nama Belakang hanya boleh diisi dengan karakter alfabetik (A-Z)',    
    'last_name_max' => 'Nama Belakang tidak boleh lebih dari :max karakter',
    'email_required' => 'Alamat Email harus diisi',
    'email_email' => 'Alamat Email tidak valid',
    'email_unique' => 'Alamat Email Anda sudah terdaftar sebelumnya',
    'sex_required' => 'Jenis Kelamin harus dipilih',
    'password_required' => 'Password/Kata Sandi harus diisi',
    'password_confirmation_required' => 'Konfirmasi Password/Kata Sandi harus diisi',
    'password_confirmed' => 'Konfirmasi Password tidak sama dengan Password',
    
    'registration_failed' => 'Maaf, akun Anda tidak dapat kami daftarkan saat ini. Mohon mencoba lagi dalam beberapa saat.',
    
    'activation_notification' => 'Selamat, Anda tinggal selangkah lagi bergabung dengan :app_name! Silakan mengaktifkan akun Anda melalui email yang telah kami kirim ke inbox Anda.',
    'activation_invalid_url' => 'URL aktivasi akun Anda tidak valid. Silakan hubungi kami di :support_email_link untuk bantuan lebih lanjut.',
    'activation_user_id_not_found' => 'Akun Anda tidak ditemukan. Silakan mendaftar menggunakan form di bawah ini.',
    'activation_user_is_activated' => 'Akun Anda sudah pernah diaktivasi sebelumnya. Silakan login menggunakan form di bawah ini.',
    'activation_unsuccessful' => 'Akun Anda tidak dapat diaktifkan saat ini. Mohon mencoba lagi dalam beberapa saat.',
    'activation_successful' => 'Selamat! Akun :app_name Anda telah aktif! Silakan login menggunakan form di bawah ini untuk mulai menggunakan :app_name.',
    
    /* -----------------------------------------------------------------------
     * emails
     * -----------------------------------------------------------------------
     */
    
    'activation_email_subject' => 'Mohon Konfirmasi Alamat Email Anda - :app_name',
    'activation_email_content_editor' => '
<p>Halo :name!</p>

<p>Selamat bergabung dengan :app_name! </p>

<p>Sebelum Anda dapat login ke :app_name, silakan klik tautan di bawah ini untuk mengaktifkan akun Anda. Setelahnya Anda dapat masuk/login ke :app_name dan mulai mengedit dokumen-dokumen yang telah diunggah!</p>

<p>:activation_link</p>

<p>Apabila Anda mengalami kesulitan, jangan ragu-ragu untuk menghubungi kami. </p>
<br/><br/>
<p>Salam,</p>
<br/>
<p>Tim :app_name</p>
',
    'activation_email_content_author' => '
<p>Halo :name!</p>

<p>Selamat bergabung dengan :app_name! </p>

<p>Sebelum Anda dapat login ke :app_name, silakan klik tautan di bawah ini untuk mengaktifkan akun Anda. Setelahnya Anda dapat masuk/login ke :app_name dan mulai mengunggah dokumen Anda!</p>

<p>:activation_link</p>

<p>Apabila Anda mengalami kesulitan, jangan ragu-ragu untuk menghubungi kami. </p>
<br/><br/>
<p>Salam,</p>
<br/>
<p>Tim :app_name</p>
',
    
    'welcome_email_subject' => 'Selamat Datang di :app_name!',
    'welcome_email_content_editor' => '
<p>Halo :name!</p>

<p>Atas nama keluarga besar :app_name, kami ingin mengucapkan selamat bergabung dengan kami! </p>

<p>Silakan login di halaman ini untuk mulai mengedit dokumen-dokumen:</p>

<p>:login_link</p>
<p><b>Username:</b> :username</p>

<p>Apabila Anda mengalami kesulitan, jangan ragu-ragu untuk menghubungi kami. </p>
<br/><br/>
<p>Salam,</p>
<br/>
<p>Tim :app_name</p>
',
    'welcome_email_content_author' => '
<p>Halo :name!</p>

<p>Atas nama keluarga besar :app_name, kami ingin mengucapkan selamat bergabung dengan kami! </p>

<p>Silakan login di halaman ini untuk mulai mengunggah dokumen Anda:</p>

<p>:login_link</p>
<p><b>Username:</b> :username</p>

<p>Apabila Anda mengalami kesulitan, jangan ragu-ragu untuk menghubungi kami. </p>
<br/><br/>
<p>Salam,</p>
<br/>
<p>Tim :app_name</p>
',
);
