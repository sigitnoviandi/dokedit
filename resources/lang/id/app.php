<?php

/*
  |--------------------------------------------------------------------------
  | Global App Language
  |--------------------------------------------------------------------------
  |
  | The following language lines are used globally by the app.
  |
 */

return array(
    
    /* -----------------------------------------------------------------------
     * APP INFO
     * -----------------------------------------------------------------------
     */    
    'app_name' => 'DokEdit',
    'app_domain' => 'dokedit.com',
    
    /* -----------------------------------------------------------------------
     * EMAIL ADDRESSES
     * -----------------------------------------------------------------------
     */    
    'email_noreply' => 'noreply@dokedit.com',
    'email_info' => 'info@dokedit.com',
    
    /* -----------------------------------------------------------------------
     * DATES
     * -----------------------------------------------------------------------
     */    
    'date_format' => 'j M Y',
    'strftime_date_format' => '%e %b %Y',
    'long_months' => array(
        'January' => 'Januari', 
        'February' => 'Februari', 
        'March' => 'Maret', 
        'May' => 'Mei', 
        'June' => 'Juni', 
        'July' => 'Juli', 
        'August' => 'Agustus', 
        'October' => 'Oktober', 
        'December' => 'Desember'
    ),
    'short_months' => array(
        'May' => 'Mei', 
        'Aug' => 'Ags', 
        'Oct' => 'Okt', 
        'Dec' => 'Des'
    ),
    
    /* -----------------------------------------------------------------------
     * ERROR PAGES
     * -----------------------------------------------------------------------
     */    
    '403_title' => 'Unauthorized Access!',
    '403_content' => 'Anda tidak dapat mengakses halaman ini.<br/>Mohon hubungi :support_email_link untuk bantuan lebih lanjut.',
    '404_title' => 'Halaman Tidak Ditemukan!',
    '404_content' => 'Halaman yang Anda cari tidak ditemukan.<br/>Mohon hubungi :support_email_link untuk bantuan lebih lanjut.'
);
