/* 
 * Registration
 * 
 * @author      : Sigit Noviandi
 * @copyright   : Diffrnt Digital
 * @package     : DokEdit
 * @since       : June 2018
 */

jQuery(function () {
    DE_Register.init();
});

var DE_Register = {

    validator: DE_FormValidator,

    init: function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        this.validator.init_form($('#registration-form'));
        
        $('#type').on('change', function(evt){
            var val = $(this).val();

            if(val == 'editor'){
                $('.editor-data').css('display', 'block');
                $('.author-data').css('display', 'none');
            } else {
                $('.editor-data').css('display', 'none');
                $('.author-data').css('display', 'block');
            }
        });
        
        if($('#type').val() == 'editor'){
            $('.editor-data').css('display', 'block');
            $('.author-data').css('display', 'none');
        } else {
            $('.editor-data').css('display', 'none');
            $('.author-data').css('display', 'block');
        }
        
        $('#skill').on('change', function(evt){
            var val = $(this).val();
            if(val == 'substansi'){
                $('#subskills-wrapper').css('display', 'block');
            } else {
                $('#subskills-wrapper').css('display', 'none');
            }
        });
        
        if($('#skill').val() == 'substansi'){
            $('#subskills-wrapper').css('display', 'block');
        } else {
            $('#subskills-wrapper').css('display', 'none');
        }
        
        $('#add-other-subskill').on('click', function(){
            var new_subskill = $('#subskills-wrapper').find('.other-subskill-sample').clone().insertBefore($('#add-other-subskill')).val('').removeClass('other-subskill-sample');
        });
    }
};





