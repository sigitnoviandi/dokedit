/* 
 * Document JS Library
 * 
 * @author:     Sigit N
 * @since:      April 2018
 * @package:    DokEdit
 * @copyright:  Diffrnt Digital
 */

jQuery(function ($) {
    Document.init();
});

var Document = {
    init: function () {
        if (/document\/add/.test(window.location)) {
            Document.Form.init();
        } else if (/document\/edit/.test(window.location)) {
            Document.FormEdit.init();
        } else if(/document\/manage\/available/.test(window.location)) {
            Document.ManageForEditor.init('available');
        } else if(/document\/manage\/in_progress/.test(window.location)) {
            Document.ManageForEditor.init('in_progress');
        } else if(/document\/manage\/completed/.test(window.location)) {
            Document.ManageForEditor.init('completed');
        } else if(/document\/submit_for_review/.test(window.location)) {
            Document.SubmitForReview.init_form();
        } else {
            Document.Manage.init();
        }
    }
};

Document.Form = {
    init: function () {
        $('#document_type').on('change', function (evt) {
            Document.Form.display_cost();
        });

        $('#no_of_pages').on('keydown', function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                            // Allow: home, end, left, right, down, up
                                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                        // let it happen, don't do anything

                        Document.Form.display_cost();
                        return;
                    }

                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    } else {
                        Document.Form.display_cost();
                    }
                });

        $('#no_of_pages').on('change', function (e) {
            Document.Form.display_cost();
        });

        $('#services_type_ejaan').on('ifChanged', function () {
            Document.Form.display_cost();
        });

        $('#services_type_tatabahasa').on('ifChanged', function () {
            Document.Form.display_cost();
        });

        $('#services_type_substansi').on('ifChanged', function () {
            Document.Form.display_cost();
        });

        $('#services_delivery_regular').on('ifChanged', function () {
            Document.Form.display_cost();
        });

        $('#services_delivery_express').on('ifChanged', function () {
            Document.Form.display_cost();
        });

        $('#btn_reset').on('click', function () {
            $('#form_new_document')[0].reset();
        });

        $('#btn_save').on('click', function () {
            Document.Form.save();
        });

        Document.Form.display_cost();
    },

    save: function () {
        var error_msg = Document.Form.validate();

        if (error_msg != '') {
            alert(error_msg);
            return false;
        }

        $('#form_new_document').submit();
    },

    validate: function () {
        var error_msg = '';

        if ($('#no_of_pages').val() == 0) {
            error_msg += '- jumlah halaman tidak boleh kosong' + "\r\n";
        }
        if ($('#document_file').val() == 0) {
            error_msg += '- file dokumen harus diupload';
        } else {
            error_msg += Document.Form.validate_uploaded_file();
        }

        if (error_msg != '') {
            error_msg = 'Mohon perbaiki beberapa kesalahan berikut: ' + "\r\n" + error_msg;
        }

        return error_msg;
    },

    validate_uploaded_file: function () {
        var error_msg = '';
        var file = $('#document_file').val();

        var text_file_extensions = ['doc', 'docx', 'odt', 'txt', 'rtf', 'tex', 'wpd', 'wks', 'wps', 'xls', 'xlsx', 'ods', 'xlr'];
        var graphic_file_extensions = ['jpg', 'jpeg', 'gif', 'png', 'tif', 'tiff', 'pdf', 'bmp', 'ppt', 'odt', 'pptx', 'key', 'xlr'];

        var found = false;

        if ($('#document_type').val() == 'text') {
            jQuery.each(text_file_extensions, function (i, ext) {
                var regex = new RegExp("\." + ext + '$', 'i');
                if (regex.test(file) == true) {
                    found = true;
                    return false;
                }
            });
        } else {
            jQuery.each(graphic_file_extensions, function (i, ext) {
                var regex = new RegExp("\." + ext + '$', 'i');
                if (regex.test(file) == true) {
                    found = true;
                    return false;
                }
            });
        }

        if (found == false) {
            error_msg = '- jenis file yang Anda upload tidak sesuai' + "\r\n";
        }

        return error_msg;
    },

    display_cost: function () {
        var service_item_text = Document.Form.construct_item_text();
        $('#service_item_text').html(service_item_text);

        var cost = Document.Form.calculate_cost();
        var tax = 0.02 * cost;
        var total = tax + cost;

        cost = 'Rp ' + number_format(cost, 0, ',', '.');
        tax = 'Rp ' + number_format(tax, 0, ',', '.');
        total = 'Rp ' + number_format(total, 0, ',', '.');

        $('#subtotal_row').html(cost);
        $('#subtotal').html(cost);
        $('#tax').html(tax);
        $('#total').html(total);


    },

    calculate_cost: function () {
        var cost = 0;
        var unit_cost = 0;

        var editing_type = 'ejaan';
        if ($('#services_type_tatabahasa').is(':checked') == true) {
            editing_type = 'tatabahasa';
        } else if ($('#services_type_substansi').is(':checked') == true) {
            editing_type = 'substansi';
        }

        if ($('#document_type').val() == 'text') {
//            unit_cost = 5000;
            if (editing_type == 'ejaan') {
                unit_cost = 3000;
            } else if (editing_type == 'tatabahasa') {
                unit_cost = 4000;
            } else {
                unit_cost = 5000;
            }
        } else {
//            unit_cost = 10000;
            if (editing_type == 'ejaan') {
                unit_cost = 8000;
            } else if (editing_type == 'tatabahasa') {
                unit_cost = 9000;
            } else {
                unit_cost = 10000;
            }
        }

        var no_of_pages = $('#no_of_pages').val();

        cost = unit_cost * no_of_pages;

        if ($('#services_delivery_express').is(':checked') == true) {
            cost = 2 * cost;
        }

        return cost;
    },

    construct_item_text: function () {
        var text = 'Edit';

        if ($('#document_type').val() == 'text') {
            text += ' naskah';
        } else {
            text += ' grafis';
        }

        text += ' <b>' + $('#no_of_pages').val() + ' halaman</b>';

        if ($('#services_delivery_express').is(':checked') == true) {
            text += ' secara <b>ekspress</b>';
        }

        return text;
    }


};

Document.FormEdit = {
    init: function () {
        $('#btn_reset').on('click', function () {
            $('#form_edit_document')[0].reset();
        });

        $('#btn_save').on('click', function () {
            Document.FormEdit.save();
        });
    },

    save: function () {
        var error_msg = Document.FormEdit.validate();

        if (error_msg != '') {
            alert(error_msg);
            return false;
        }

        $('#form_edit_document').submit();
    },

    validate: function () {
        var error_msg = '';

        if ($('#title').val() == '') {
            error_msg += '- judul dokumen tidak boleh kosong' + "\r\n";
        }

        if ($('#document_id').val() == '') {
            error_msg += '- dokumen ID tidak valid';
        }

        if (error_msg != '') {
            error_msg = 'Mohon perbaiki beberapa kesalahan berikut: ' + "\r\n" + error_msg;
        }

        return error_msg;
    }
};

Document.Manage = {
    init: function () {
        DE_Table.init();
        
        $('.table-container').find('.action-buttons').find('.btn-cancel').on('click', Document.Manage.cancel);
        
        $('.table-container').find('.action-buttons').find('.btn-mark-as-complete').on('click', Document.Manage.mark_as_complete);
        
        $('.table-container').find('.action-buttons').find('.btn-confirm-payment').on('click', Document.Manage.confirm_payment);
        
        $('.table-container').find('.btn-view-payment-confirmation').on('click', Document.Manage.view_payment_confirmation);
        
        $('.table-container').find('.btn-view-editor-notes').on('click', Document.Manage.view_editor_notes);
    },

    cancel: function (evt) {
        var record_name = $(this).attr('data-record-name');
        if (!confirm('Anda yakin untuk membatalkan proses edit dokumen "' + record_name + '"?')) {
            evt.preventDefault();
            return false;
        }
    },
    
    mark_as_complete: function (evt) {
        var record_name = $(this).attr('data-document-name');
        if (!confirm('Anda yakin untuk menyelesaikan proses edit dokumen "' + record_name + '"?')) {
            evt.preventDefault();
            return false;
        }
    },

    confirm_payment: function (evt) {
        if (!confirm('Anda yakin untuk mengkonfirmasi pembayaran untuk dokumen ' + $(this).data('record-name') + '?')) {
            return false;
        }

        $(this).hide();

        var loader = $('<span><i class="fa fa-spinner fa-pulse fa-lg"></i> processing...</span>');
        loader.insertAfter($(this));

        var data = {
            document_id: $(this).data('document-id')
        };

        var element = $(this);

        $.post(AJAX_URL.confirm_payment, data, function (response) {
            loader.remove();
            element.show();

            if (response.success == true) {
                alert(response.msg);
            } else {
                alert(response.err_msg);
            }
            
            window.location.reload();
        }, 'json');
    },

    view_payment_confirmation: function (evt) {
        $(this).hide();
            
        var loader = $('<span><i class="fa fa-spinner fa-pulse fa-lg"></i> loading...</span>');
        loader.insertAfter($(this));

        var data = {
            document_id: $(this).data('document-id')
        };

        var element = $(this);

        $.post(AJAX_URL.get_payment_confirmation_detail, data, function (response) {
            loader.remove();
            element.show();

            if (response.success == true) {
                var modal = $('#modal-payment-confirmation');
                var payment_confirmation_detail = Document.Manage.build_payment_confirmation_detail(response.confirmation);
                
                modal.find('.modal-body').html(payment_confirmation_detail);
                modal.modal('show');
            } else {
                alert(response.err_msg);
            }
        }, 'json');
    },
    
    view_editor_notes: function (evt) {
        $(this).hide();
            
        var loader = $('<span><i class="fa fa-spinner fa-pulse fa-lg"></i> loading...</span>');
        loader.insertAfter($(this));

        var data = {
            document_id: $(this).data('document-id')
        };

        var element = $(this);

        $.post(AJAX_URL.get_editor_notes, data, function (response) {
            loader.remove();
            element.show();

            if (response.success == true) {
                var modal = $('#modal-editor-notes');
                modal.find('.modal-body').html(response.editor_notes);
                modal.modal('show');
            } else {
                alert(response.err_msg);
            }
        }, 'json');
    },
    
    build_payment_confirmation_detail: function(detail){
        var confirmation_detail = ' <table>';
        confirmation_detail += '        <tr><td>Metode</td><td>: ' + detail.method.replace('_', ' ') + '</td></tr>';
        confirmation_detail += '        <tr><td>Dibayar ke</td><td>: ' + detail.paid_to + '</td></tr>';
        confirmation_detail += '        <tr><td>Jumlah</td><td>: ' + detail.amount + '</td></tr>';
        confirmation_detail += '        <tr><td>No. Rekening</td><td>: ' + detail.account_owner_no + '</td></tr>';
        confirmation_detail += '        <tr><td>Nama Pemilik Rekening</td><td>: ' + detail.account_owner_name + '</td></tr>';
        confirmation_detail += '        <tr><td>Tanggal Pembayaran</td><td>: ' + detail.payment_date + '</td></tr>';
        confirmation_detail += '        <tr><td>Bukti Pembayaran</td><td>: <a href="' + detail.screenshot + '"><img src="' + detail.screenshot + '" style="max-width: 200px;"/></a></td></tr>';
        confirmation_detail += '    </table>';
        
        return confirmation_detail;
    }
};

Document.ManageForEditor = {
    init: function (action) {
        DE_Table.init();
        
        if(action == 'available'){
            $('.table-container').find('.action-buttons').find('.btn-take').on('click', Document.ManageForEditor.take);
        }
        
    },
    
    take: function(evt){
        var record_name = $(this).attr('data-record-name');
        if (!confirm('Anda yakin ingin mengambil dokumen "' + record_name + '" untuk diedit?')) {
            evt.preventDefault();
            return false;
        }
    }
};

Document.SubmitForReview = {
    init_form: function () {
        $('#btn_reset').on('click', function () {
            $('#form_document_submit_for_review')[0].reset();
        });

        $('#btn_save').on('click', function () {
            Document.SubmitForReview.save();
        });
    },

    save: function () {
        var error_msg = Document.SubmitForReview.validate();

        if (error_msg != '') {
            alert(error_msg);
            return false;
        }

        $('#form_document_submit_for_review').submit();
    },

    validate: function () {
        var error_msg = '';

        if($('#filename_revision').length > 0){
            if ($('#filename_final').val() == 0) {
                error_msg += '- file dengan catatan revisi harus diupload' + "\r\n";
            } else {
            }
        }
        if ($('#filename_final').val() == 0) {
            error_msg += '- file dokumen final yang telah diperbaiki harus diupload' + "\r\n";
        } else {
//            error_msg += Document.SubmitForReview.validate_uploaded_file();
        }

        if (error_msg != '') {
            error_msg = 'Mohon perbaiki beberapa kesalahan berikut: ' + "\r\n" + error_msg;
        }

        return error_msg;
    }
};


