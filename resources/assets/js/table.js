/*
 *   DokEdit Common Table JS Library
 *   
 *   @author     : Sigit Noviandi
 *   @copyright  : Diffrnt Digital.
 *   @package    : DokEdit
 *   @since      : April 2018
 */

var DE_Table = {
    init: function(){
        DE_Table.init_filter();
        DE_Table.init_action_buttons();
        DE_Table.init_record_checkbox();
    },
    
    init_filter: function(){
        $('#btn-search').on('click', function(){
            $('#filter-form').submit();
        });
        $('#filter-form').find('input[type="text"]').on('keyup', function(evt){
            if(evt.keyCode == 13){
                $('#filter-form').submit();
            }
        });
        $('#filter-limit-options').on('change', function(){
            $('#filter-limit').val($('#filter-limit-options').val());
            $('#filter-form').submit();
        });
    },
    
    init_action_buttons: function(){
        $('.table-container').find('.action-buttons').find('.btn-remove').on('click', function(evt){
            var record_name = $(this).attr('data-record-name');
            if(!confirm('Anda yakin untuk menghapus "' + record_name + '"?')){
                evt.preventDefault();
                return false;
            }
        });
    },
    
    init_record_checkbox: function(){
        
        if($('.record-checkbox-toggle')){
            $('.record-checkbox-toggle').on('ifToggled', function(){
                if($('.record-checkbox-toggle').is(':checked')){
                    $('.record-checkbox').iCheck('check');
                } else {
                    $('.record-checkbox').iCheck('uncheck');
                }
            });
        }
    },
    
    get_checked_records: function(){
        var record_checkboxes = $('.record-checkbox');
        var records = new Array;
        $.each(record_checkboxes, function(i, e){
            if($(e).is(':checked')){
                records.push($(e).val());
            }
        });
        return records;
    }
};
