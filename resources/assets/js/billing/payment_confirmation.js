/* 
 * Payment Confirmation JS Library
 * 
 * @author:     Sigit N
 * @since:      June 2018
 * @package:    DokEdit
 * @copyright:  Diffrnt Digital
 */

jQuery(function ($) {
    PaymentConfirmation.init();
});

var PaymentConfirmation = {
    init: function(){
        $('#payment_date').datepicker({
            autoclose: true,
            setDate: new Date(),
            format: 'yyyy-mm-dd'
        });
        
        $('#btn_save').on('click', function () {
            PaymentConfirmation.save();
        });
        
    },
    
    save: function () {
        var error_msg = PaymentConfirmation.validate();

        if (error_msg != '') {
            alert(error_msg);
            return false;
        }

        $('#form_payment_confirmation').submit();
    },

    validate: function () {
        var error_msg = '';

        if ($('#amount').val() == 0) {
            error_msg += '- jumlah pembayaran tidak boleh kosong' + "\r\n";
        }
        if ($('#account_owner_no').val() == 0) {
            error_msg += '- no rekening tidak boleh kosong' + "\r\n";
        }
        if ($('#account_owner_name').val() == 0) {
            error_msg += '- nama pemilik rekening tidak boleh kosong' + "\r\n";
        }
        if ($('#payment_date').val() == 0) {
            error_msg += '- tanggal pembayaran tidak boleh kosong' + "\r\n";
        }
        if ($('#payment_screenshot').val() == 0) {
            error_msg += '- bukti transfer harus diupload';
        }

        if (error_msg != '') {
            error_msg = 'Mohon perbaiki beberapa kesalahan berikut: ' + "\r\n" + error_msg;
        }

        return error_msg;
    },
};


