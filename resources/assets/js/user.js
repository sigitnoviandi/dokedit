/* 
 * User page Common JS Library
 * 
 * @author      : Sigit Noviandi
 * @copyright   : Diffrnt Digital
 * @package     : DokEdit
 * @since       : June 2018
 */

jQuery(function ($) {
    if (/admin\/manage/.test(window.location)) {
        User.Manage.init();
    } else if (/admin\/manage/.test(window.location)) {
        User.Form.init();
    } else if (/profile/.test(window.location)) {
        User.Profile.init();
    }
});

var User = {
    Manage: {
        init: function () {
            // init table
            DE_Table.init();

            $('.btn-activate-user').on('click', User.Manage.activate_user);

            $('.btn-resend-activation').on('click', User.Manage.resend_activation);
        },

        activate_user: function () {
            if (!confirm('Are you sure to activate user ' + $(this).data('user-name') + '?')) {
                return false;
            }

            $(this).hide();

            var loader = $('<span><i class="fa fa-spinner fa-pulse fa-lg"></i> activating...</span>');
            loader.insertAfter($(this));

            var data = {
                user_id: $(this).data('user-id')
            };

            var element = $(this);

            $.post(AJAX_URL.activate_user, data, function (response) {
                loader.remove();

                if (response.success == true) {
                    element.parent().html('<span class="label label-success">Active</span>');
                } else {
                    element.show();
                    alert(response.err_msg);
                }
            }, 'json');
        },

        resend_activation: function () {
            if (!confirm('Are you sure to resend the activation email to user ' + $(this).data('user-name') + '?')) {
                return false;
            }

            $(this).hide();

            var loader = $('<span><i class="fa fa-spinner fa-pulse fa-lg"></i> sending...</span>');
            loader.insertAfter($(this));

            var data = {
                user_id: $(this).data('user-id')
            };

            var element = $(this);

            $.post(AJAX_URL.resend_activation, data, function (response) {
                loader.remove();
                element.show();

                if (response.success == true) {
                    alert(response.msg);
                } else {
                    alert(response.err_msg);
                }
            }, 'json');
        }
    },

    Form: {
        init: function () {

        }
    },

    Profile: {

        init: function () {
            $('#btn-show-edit-form').on('click', function () {
                $('#user-profile-forms-tab a').trigger('click');
            });
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });

            $('#skill').on('change', function (evt) {
                var val = $(this).val();
                if (val == 'substansi') {
                    $('#subskills-wrapper').css('display', 'block');
                } else {
                    $('#subskills-wrapper').css('display', 'none');
                }
            });

            if ($('#skill').val() == 'substansi') {
                $('#subskills-wrapper').css('display', 'block');
            } else {
                $('#subskills-wrapper').css('display', 'none');
            }

            $('#add-other-subskill').on('click', function () {
                var new_subskill = $('#subskills-wrapper').find('.other-subskill-sample').clone().insertBefore($('#add-other-subskill')).val('').removeClass('other-subskill-sample');
            });
        }
    },

    Dashboard: {
        init: function () {
            var documents_pie_chart_canvas = $('#documents_pie_chart').get(0).getContext('2d');
            var documents_pie_chart = new Chart(documents_pie_chart_canvas);
            documents_pie_chart.Doughnut(documents_pie_data);
            
            var users_pie_chart_canvas = $('#users_pie_chart').get(0).getContext('2d');
            var users_pie_chart = new Chart(users_pie_chart_canvas);
            users_pie_chart.Doughnut(users_pie_data);

        }
    }
};