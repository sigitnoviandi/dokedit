<!-- general form elements -->
<!-- form start -->
<form role="form" id="form_payment_confirmation" action="{{ URL::Route('billing.payment_confirmation.save') }}" method="POST" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-default">
                <div class="box-body action-wrapper">  
                    <a class="btn btn-default pull-right" href="{{ URL::Route('document.manage.get') }}" id="btn_cancel">Batal</a>
                    <a class="btn btn-primary pull-right" id="btn_save"><i class="fa fa-save"></i> Simpan</a>
                    <span id="loading-indicator" style="display:none;" class="loading-indicator pull-right inactive"><i class="fa fa-spinner fa-spin fa-pulse fa-lg"></i></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Konfirmasi Pembayaran Dokumen</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool btn-box-tool-large" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="method">Metode Pembayaran *</label>
                                <select name="method" id="method" class="form-control">
                                    <option value="bank_transfer">Transfer</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="paid_to">Transfer ke Rekening *</label>
                                <select name="paid_to" id="paid_to" class="form-control">
                                    <option>Bank BCA No. Rek xxxxxx a/n DokEdit</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="amount">Jumlah yang Dibayar *</label>
                                <input type="text"class="form-control" name="amount" id="amount" value="{{ $document->cost(FALSE) }}" required/>
                            </div>
                            <div class="form-group">
                                <label for="account_owner_no">Nomor Rekening *</label>
                                <input type="text"class="form-control" name="account_owner_no" id="account_owner_no" required/>
                            </div>
                            <div class="form-group">
                                <label for="account_owner_name">Nama Pemilik Rekening *</label>
                                <input type="text"class="form-control" name="account_owner_name" id="account_owner_name" required/>
                            </div>
                            <div class="form-group">
                                <label for="payment_date">Tanggal Pembayaran *</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="payment_date" name="payment_date" value="{{ gmdate('Y-m-d') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="payment_screenshot">Bukti Transfer *</label>
                                <input type="file" name="payment_screenshot" id="payment_screenshot" required/>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                
                <input type="hidden" name="document_id" value="{{ $document->id }}"/>
                <input type="hidden" name="action" value="edit"/>
            </div><!-- /.box -->
        </div>
        <div class="col-lg-4 col-md-4"></div>        
    </div>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="document_id" name="document_id" value="{{ isset($document->id) && !empty($document->id) ? $document->id : ''}}">
</form>