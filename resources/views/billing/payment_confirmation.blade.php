@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    Konfirmasi Pembayaran
@endsection

@section('contentheader_title')
    Konfirmasi Pembayaran
@endsection

@section('contentheader_description')
    Kirim konfirmasi pembayaran untuk pengerjaan editing dokumen "{{ $document->title }}"
@endsection

@section('page_breadcrumbs')
    {!! Breadcrumbs::render('payment_confirmation', $document->id) !!}
@endsection

@section('custom-css')
<link rel="stylesheet" href="{{ asset('plugins/datepicker/datepicker3.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/timepicker/bootstrap-timepicker.min.css') }}">
@endsection

@section('main-content')
<!-- page buttons -->
<div class="row">
    <div class="col-lg-"></div>
</div>
<!-- end of page buttons -->

<!-- alert -->
@if(count(Alert::get()) > 0)        
    @foreach (Alert::get() as $alert)
        <div class="alert alert-{{ $alert->class }} alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p>{{ $alert->message }}</p>
        </div>
    @endforeach                    
@endif
<!-- end of alert -->

<!-- page table -->
@include('billing.partials.payment_confirmation_form')
<!-- end of page table -->
@endsection

@section('custom-js')
<script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
<script src="{{ asset('js/billing/payment_confirmation.min.js') }}"></script>
@endsection