<div class="box table-container">
    <div class="box-header">
        Menampilkan 
        <select name="limit" id="filter-limit-options" class="limit-options">
            <option value="10" @if(isset($filter['filter_limit']) && $filter['filter_limit'] == 10) selected @endif>10</option>
            <option value="20" @if(isset($filter['filter_limit']) && $filter['filter_limit'] == 20) selected @endif>20</option>
            <option value="40" @if(isset($filter['filter_limit']) && $filter['filter_limit'] == 40) selected @endif>40</option>
            <option value="50" @if(isset($filter['filter_limit']) && $filter['filter_limit'] == 50) selected @endif>50</option>
            <option value="100" @if(isset($filter['filter_limit']) && $filter['filter_limit'] == 100) selected @endif>100</option>
        </select> baris
        <div class="box-tools">
            @include('pagination.default', ['paginator' => $documents])
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding">
        <form id="filter-form" method="post" action="{{ URL::Route('document.filter.set.post') }}">            
            <table class="table table-striped table-hover table-bordered">
                <tr>
                    <th style="width: 2%;">#</th>
                    <th style="width: 15%;">Judul</th>
                    <th style="width: 3%;">Jml Hal.</th>
                    <th style="width: 15%;">Jenis Layanan</th>
                    <th style="width: 15%;">Kecepatan Layanan</th>
                    <th style="width: 10%;">Tanggal Masuk</th>
                    @if(isset($action) && $action == 'in_progress')
                    <th style="width: 10%;">Tanggal Mulai Diedit</th>
                    @endif
                    @if(isset($action) && $action == 'completed')
                    <th style="width: 10%;">Status</th>
                    @endif
                    <th style="width: 10%;">Pemilik</th>
                    <th style="width: 20%;">&nbsp;</th>
                </tr>
                <tr class="filter">
                    <td>&nbsp;</td>
                    <td><input type="text" name="filter_title" class="form-control input-sm" placeholder="Cari Judul" value="{{ isset($filter['filter_title']) ? $filter['filter_title'] : '' }}"/></td>
                    <td><input type="text" name="filter_no_of_pages" class="form-control input-sm" placeholder="Cari Jml Hal." value="{{ isset($filter['filter_no_of_pages']) ? $filter['filter_no_of_pages'] : '' }}"/></td>
                    <td>&nbsp;</>
                    <td>
                        <select name="filter_delivery_speed" class="form-control" id="filter_delivery_speed">
                            <option value="">Cari Kecepatan Layanan</option>
                            <option value="regular" <?php if(isset($filter['filter_delivery_speed']) && $filter['filter_delivery_speed'] == 'regular') echo 'selected'; ?>>Reguler</option>
                            <option value="express" <?php if(isset($filter['filter_delivery_speed']) && $filter['filter_delivery_speed'] == 'express') echo 'selected'; ?>>Ekspres</option>
                        </select>
                    </td>
                    @if(isset($action) && $action == 'in_progress')
                    <td>&nbsp;</td>
                    @endif
                    <td>&nbsp;</td>
                    <td><input type="text" name="filter_author" class="form-control input-sm" placeholder="Cari Pemilik" value="{{ isset($filter['filter_author']) ? $filter['filter_author'] : '' }}"/></td>
                    <td>&nbsp;</td>
                </tr>
                @if(isset($documents) && !empty($documents))
                    @foreach($documents as $document)
                        <tr>
                            <td>{{ $row_no++ }}</td>
                            <td>{{ $document->title }}</td>
                            <td>{{ $document->no_of_pages }}</td>
                            <td>{{ $document->type() }}</td>
                            <td>{{ $document->delivery_speed() }}</td>
                            <td>{{ $document->available_since() }}</td>
                            @if(isset($action) && $action == 'in_progress')
                            <td>{{ $document->edited_since() }}</td>
                            @endif
                            @if(isset($action) && $action == 'completed')
                            <td>
                                @if($document->status == 'in_review')
                                    <span class="label label-warning">Sedang Direview</span>
                                @elseif($document->status == 'completed')
                                    <span class="label label-success">Selesai</span>
                                @endif
                            </td>
                            @endif
                            <td><a href="#">{{ $document->author_name() }}</a></td>
                            <td class="action-buttons">
                                @if(isset($action) && $action == 'available')
                                <a href="{{ URL::Route('document.take.get', [$document->id]) }}" data-record-name="{{ $document->title }}" class="btn btn-default btn-sm btn-take" title="Ambil Dokumen ini Untuk Diedit"><i class="fa fa-pencil"></i> Ambil</a>
                                @elseif(isset($action) && $action == 'in_progress')
                                <a href="{{ URL::Route('document.download.get', [$document->id]) }}" data-record-name="{{ $document->title }}" class="btn btn-default btn-sm btn-download" title="Unduh File Dokumen ini Untuk Diedit"><i class="fa fa-download"></i> Unduh</a>
                                <a href="{{ URL::Route('document.submit_for_review.get', [$document->id]) }}" data-record-name="{{ $document->title }}" class="btn btn-default btn-sm btn-submit" title="Unggah Dokumen Hasil Edit"><i class="fa fa-upload"></i> Unggah Hasil Edit</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                <tr>
                    <td colspan="6">No records found.</td>
                </tr>
                @endif
            </table>
            <input type="hidden" value="10" name="filter_limit" id="filter-limit"/>
            <input type="hidden" value="{{ $action }}" name="action" id="filter-action"/>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>
    </div>
    <!-- /.box-body -->
    <div class="box-footer clearfix">
        <span class="no-of-records">Menampilkan baris {{ $first_record_no }}&ndash;{{ $row_no - 1 }} dari total {{ number_format($documents->total()) }} baris.</span>
        @include('pagination.default', ['paginator' => $documents])
    </div>
</div>
<!-- /.box -->