<!-- general form elements -->
<!-- form start -->
<form role="form" id="form_edit_document" action="{{ URL::Route('document.save.post') }}" method="POST" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-default">
                <div class="box-body action-wrapper">  
                    <a class="btn btn-danger pull-right" id="btn_reset">Reset</a>
                    <a class="btn btn-default pull-right" href="{{ URL::Route('document.manage.get') }}" id="btn_cancel">Batal</a>
                    <a class="btn btn-primary pull-right" id="btn_save"><i class="fa fa-save"></i> Simpan</a>
                    <span id="loading-indicator" style="display:none;" class="loading-indicator pull-right inactive"><i class="fa fa-spinner fa-spin fa-pulse fa-lg"></i></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Informasi Dokumen</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool btn-box-tool-large" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="title">Judul Dokumen *</label>
                                <input type="text" name="title" id="title" class="form-control" value="{{ $document->title }}"/>
                            </div>
                            <div class="form-group">
                                <label for="notes">Catatan <em>(optional)</em></label>
                                <textarea class="form-control" name="notes" id="notes">{{ $document->notes }}</textarea>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                
                <input type="hidden" name="document_id" value="{{ $document->id }}"/>
                <input type="hidden" name="action" value="edit"/>
            </div><!-- /.box -->
        </div>
        <div class="col-lg-4 col-md-4"></div>        
    </div>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="user_id" name="user_id" value="{{ isset($user->id) && !empty($user->id) ? $user->id : ''}}">
</form>