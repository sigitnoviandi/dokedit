<!-- general form elements -->
<!-- form start -->
<form role="form" id="form_new_document" action="{{ URL::Route('document.save.post') }}" method="POST" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-default">
                <div class="box-body action-wrapper">  
                    <span>Dengan menekan tombol <strong>Kirim Dokumen</strong>, Anda menyetujui jumlah biaya yang harus Anda bayar.</span>
                    <a class="btn btn-danger pull-right" id="btn_reset">Reset</a>
                    <a class="btn btn-default pull-right" href="{{ URL::Route('document.manage.get') }}" id="btn_cancel">Batal</a>
                    <a class="btn btn-primary pull-right" id="btn_save"><i class="fa fa-send"></i> Kirim Dokumen</a>
                    <span id="loading-indicator" style="display:none;" class="loading-indicator pull-right inactive"><i class="fa fa-spinner fa-spin fa-pulse fa-lg"></i></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8 col-md-8">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Langkah 1: Pilih Jenis Dokumen</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool btn-box-tool-large" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="first_name">Jenis Dokumen *</label>
                                <select name="document_type" class="form-control" id="document_type">
                                    <option value="text">Naskah/Teks</option>
                                    <option value="graphic">Gambar/Grafis/Slide Presentasi</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="first_name">Jumlah Halaman/Slide *</label>
                                <input type="number" name="no_of_pages" id="no_of_pages" class="form-control" min="1" value="1"/>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Langkah 2: Pilih Jenis dan Kecepatan Layanan</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool btn-box-tool-large" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="first_name">Jenis Layanan *</label><br/>
                                <label><input type="radio" name="services_type" id="services_type_ejaan" value="ejaan" class="icheck square-blue" checked> Periksa Kata/Ejaan</label><br/>
                                <label><input type="radio" name="services_type" id="services_type_tatabahasa" value="tatabahasa" class="icheck square-blue" checked> Periksa Tata Bahasa</label><br/>
                                <label><input type="radio" name="services_type" id="services_type_substansi" value="substansi" class="icheck square-blue" checked> Periksa Substansi</label>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="first_name">Kecepatan Layanan *</label><br/>
                                <label><input type="radio" name="services_delivery" id="services_delivery_regular" value="regular" class="icheck square-blue" checked> Reguler (3 - 6 hari per 100 halaman)</label><br/>
                                <label><input type="radio" name="services_delivery" id="services_delivery_express" value="express" class="icheck square-blue"> Ekspres (1 - 48 jam per 100 halaman)</label>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Langkah 3: Unggah Dokumen</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool btn-box-tool-large" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="first_name">Unggah Dokumen *</label>
                                <p class="text-muted"><em>File "Naskah" yang kami terima: <b>MS Word</b> (.doc, .docx, .wks, .wps), <b>MS Excel</b> (.xls, .xlsx, .xlr), <b>Plain Text Editor/Notepad</b> (.txt), <b>WordPerfect</b> (.wpd), <b>Open Office</b> (.odt, .ods), <b>Rich Text Format</b> (.rtf), <b>LaTeX</b> (.tex)</em></p>
                                <p class="text-muted"><em>File "Grafis" yang kami terima: <b>gambar/image</b> (.jpg, .jpeg, .tiff, .tif, .png, .gif, .bmp, .pdf), <b>Slide Presentasi</b> (.ppt, .pptx, .pps, .odp, .key)</em></p>
                                <input type="file" name="document_file" id="document_file" class=""/>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-lg-4 col-md-4">
            <section class="invoice invoice-without-margin">
                <!-- title row -->
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                            <i class="fa fa-calculator"></i> Perkiraan Biaya
                            <small class="pull-right">Tanggal: {{ date('j M Y')}}</small>
                        </h2>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- Table row -->
                <div class="row">
                    <div class="col-xs-12 table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Layanan</th>
                                    <th class="text-right">Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td id="service_item_text">Edit Naskah/Teks</td>
                                    <td id="subtotal_row" class="text-right"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <div class="row">
                    <!-- /.col -->
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th style="width:50%">Subtotal:</th>
                                        <td id="subtotal" class="text-right">0</td>
                                    </tr>
                                    <tr>
                                        <th>Pajak (2%)</th>
                                        <td id="tax" class="text-right">0</td>
                                    </tr>
                                    <tr>
                                        <th>Total:</th>
                                        <td id="total" class="text-right">0</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>

                <div class="row">
                    <!-- accepted payments column -->
                    <div class="col-xs-12">
                        <p class=""><h4>Metode Pembayaran</h4>Lakukan pembayaran dengan melakukan transfer ke nomor rekening berikut ini.</p>
                        <p class="text-muted well well-sm no-shadow" style="margin-bottom: 0px; margin-top: 10px;">
                            <strong>
                            Bank Mandiri<br/>
                            Cab. Jagakarsa<br/>
                            No. Rek. 987 6543 210<br/>
                            a/n DokEdit
                            </strong>
                        </p>
                    </div>
                </div>
                <!-- /.row -->
            </section>
        </div>        
    </div>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="user_id" name="user_id" value="{{ isset($user->id) && !empty($user->id) ? $user->id : ''}}">
</form>