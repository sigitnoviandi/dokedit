<div class="box table-container">
    <div class="box-header">
        Menampilkan 
        <select name="limit" id="filter-limit-options" class="limit-options">
            <option value="10" @if(isset($filter['filter_limit']) && $filter['filter_limit'] == 10) selected @endif>10</option>
            <option value="20" @if(isset($filter['filter_limit']) && $filter['filter_limit'] == 20) selected @endif>20</option>
            <option value="40" @if(isset($filter['filter_limit']) && $filter['filter_limit'] == 40) selected @endif>40</option>
            <option value="50" @if(isset($filter['filter_limit']) && $filter['filter_limit'] == 50) selected @endif>50</option>
            <option value="100" @if(isset($filter['filter_limit']) && $filter['filter_limit'] == 100) selected @endif>100</option>
        </select> baris
        <div class="box-tools">
            @include('pagination.default', ['paginator' => $documents])
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding">
        <form id="filter-form" method="post" action="{{ URL::Route('document.filter.set.post') }}">            
            <table class="table table-striped table-hover table-bordered">
                <tr>
                    <th style="width: 2%;">#</th>
                    <th style="width: 15%;">Judul</th>
                    <th style="width: 3%;">Jml Hal.</th>
                    <th style="width: 15%;">Jenis Layanan</th>
                    <th style="width: 15%;">Kecepatan Layanan</th>
                    <th style="width: 5%;">Status</th>
                    <th style="width: 5%;">Pembayaran</th>
                    <th style="width: 10%;">Biaya</th>
                    <th style="width: 10%;">Editor</th>
                    <th style="width: 20%;">&nbsp;</th>
                </tr>
                <tr class="filter">
                    <td>&nbsp;</td>
                    <td><input type="text" name="filter_title" class="form-control input-sm" placeholder="Cari Judul" value="{{ isset($filter['filter_title']) ? $filter['filter_title'] : '' }}"/></td>
                    <td><input type="text" name="filter_no_of_pages" class="form-control input-sm" placeholder="Cari Jml Hal." value="{{ isset($filter['filter_no_of_pages']) ? $filter['filter_no_of_pages'] : '' }}"/></td>
                    <td>&nbsp;</>
                    <td>&nbsp;</td>
                    <td>
                        <select name="filter_status" class="form-control" id="filter_status">
                            <option value="">Cari Status</option>
                            <option value="awaiting_payment" <?php if(isset($filter['filter_status']) && $filter['filter_status'] == 'awaiting_payment') echo 'selected'; ?>>Menunggu Pembayaran</option>
                            <option value="payment_confirmation_sent" <?php if(isset($filter['filter_status']) && $filter['filter_status'] == 'payment_confirmation_sent') echo 'selected'; ?>>Konfirmasi Pembayaran Diterima</option>
                            <option value="cancelled" <?php if(isset($filter['filter_status']) && $filter['filter_status'] == 'cancelled') echo 'selected'; ?>>Dibatalkan</option>
                            <option value="in_progress" <?php if(isset($filter['filter_status']) && $filter['filter_status'] == 'in_progress') echo 'selected'; ?>>Dalam Pengerjaan</option>
                            <option value="in_review" <?php if(isset($filter['filter_status']) && $filter['filter_status'] == 'in_review') echo 'selected'; ?>>Dalam Review</option>
                            <option value="completed" <?php if(isset($filter['filter_status']) && $filter['filter_status'] == 'completed') echo 'selected'; ?>>Selesai</option>
                        </select>
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><input type="text" name="filter_editor" class="form-control input-sm" placeholder="Cari Editor" value="{{ isset($filter['filter_editor']) ? $filter['filter_editor'] : '' }}"/></td>
                    <td>&nbsp;</td>
                </tr>
                @if(isset($documents) && !empty($documents))
                    @foreach($documents as $document)
                        <tr>
                            <td>{{ $row_no++ }}</td>
                            <td>{{ $document->title }}</td>
                            <td>{{ $document->no_of_pages }}</td>
                            <td>{{ $document->type() }}</td>
                            <td>{{ $document->delivery_speed() }}</td>
                            <td>
                                {!! $document->status() !!}
                                @if($document->status == 'payment_confirmation_sent' && $logged_user->hasAccess('billings.payment_confirmation.view'))
                                <a class="btn btn-default btn-xs btn-view-payment-confirmation btn-new-line" data-document-id="{{ $document->id }}" title="Lihat Detil Konfirmasi Pembayaran">Lihat Detil</a>
                                @endif
                            </td>
                            <td>{!! $document->payment_status() !!}</td>
                            <td>
                                {{ $document->cost() }}
                                @if($document->payment_status == 'paid')
                                <br/>
                                <span class="label label-success">{{ $document->payment_amount() }}</span>
                                @endif
                            </td>
                            <td><a href="#">{{ $document->editor_name() }}</a></td>
                            <td class="action-buttons">
                                @if($document->status == 'awaiting_payment')
                                <a href="{{ URL::Route('document.cancel.get', [$document->id]) }}" class="btn btn-danger btn-sm btn-cancel" data-record-name="{{ $document->title }}"><i class="fa fa-ban"></i> Batal</a>
                                @endif
                                @if(($logged_user->is_admin() || $document->user_id == $logged_user->id))
                                    @if($document->status == 'awaiting_payment' || $document->status == 'in_the_basket')
                                        <a href="{{ URL::Route('document.remove.get', [$document->id]) }}" class="btn btn-danger btn-sm btn-remove" data-record-name="{{ $document->title }}"><i class="fa fa-trash"></i> Hapus</a>
                                    @endif
                                <a href="{{ URL::Route('document.edit.get', [$document->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i> Edit</a>
                                @endif
                                @if(($document->status == 'in_review' || $document->status == 'completed')  && ($logged_user->is_admin() || $document->user_id == $logged_user->id))
                                    @if($document->type == 'text')
                                    <a href="{{ URL::Route('document.download.revision.get', [$document->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-download"></i> Download File Revisi</a>
                                    @endif
                                    <a href="{{ URL::Route('document.download.final.get', [$document->id]) }}" class="btn btn-success btn-sm"><i class="fa fa-download"></i> Download File Final</a>
                                    <a class="btn btn-default btn-sm btn-view-editor-notes" data-document-id="{{ $document->id }}"><i class="fa fa-eye"></i> Lihat Catatan Editor</a>
                                @endif
                                @if($document->status == 'in_review' && ($logged_user->is_admin() || $document->user_id == $logged_user->id))
                                    <a href="{{ URL::Route('document.mark_as_complete.get', [$document->id]) }}" class="btn btn-default btn-sm btn-mark-as-complete" data-document-id="{{ $document->id }}" data-document-name="{{ $document->title }}"><i class="fa fa-check"></i> Selesai</a>
                                @endif
                                @if($document->payment_status == 'unpaid' && $document->status != 'payment_confirmation_sent')
                                <a href="{{ URL::Route('billing.payment_confirmation.get', [$document->id]) }}" class="btn btn-default btn-sm" title="Kirim Konfirmasi Pembayaran untuk Dokumen Ini">Kirim Konfirmasi Pembayaran</a>
                                @endif
                                @if($logged_user->hasAccess('billings.payment.confirm') && $document->payment_status == 'unpaid')
                                <a class="btn btn-default btn-sm btn-confirm-payment" data-document-id="{{ $document->id }}" data-record-name="{{ $document->title }}" title="Konfirmasi Pembayaran">Konfirmasi Pembayaran</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                <tr>
                    <td colspan="6">No records found.</td>
                </tr>
                @endif
            </table>
            <input type="hidden" value="10" name="filter_limit" id="filter-limit"/>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>
    </div>
    <!-- /.box-body -->
    <div class="box-footer clearfix">
        <span class="no-of-records">Menampilkan baris {{ $first_record_no }}&ndash;{{ $row_no - 1 }} dari total {{ number_format($documents->total()) }} baris.</span>
        @include('pagination.default', ['paginator' => $documents])
    </div>
</div>
<!-- /.box -->