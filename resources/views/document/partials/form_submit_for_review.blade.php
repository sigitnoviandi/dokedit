<!-- general form elements -->
<!-- form start -->
<form role="form" id="form_document_submit_for_review" action="{{ URL::Route('document.submit_for_review.post') }}" method="POST" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-default">
                <div class="box-body action-wrapper">  
                    <a class="btn btn-danger pull-right" id="btn_reset">Reset</a>
                    <a class="btn btn-default pull-right" href="{{ URL::Route('document.manage_for_editor.get', ['in_progress']) }}" id="btn_cancel">Batal</a>
                    <a class="btn btn-primary pull-right" id="btn_save"><i class="fa fa-send"></i> Kirim untuk Review</a>
                    <span id="loading-indicator" style="display:none;" class="loading-indicator pull-right inactive"><i class="fa fa-spinner fa-spin fa-pulse fa-lg"></i></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">File Dokumen</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool btn-box-tool-large" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-12">
                            @if($document->type == 'text')
                            <div class="form-group">
                                <label for="title">File dengan Catatan Revisi *</label>
                                <input type="file" name="filename_revision" id="filename_revision" class=""/>
                            </div>
                            @endif
                            <div class="form-group">
                                <label for="title">File Final *</label>
                                <input type="file" name="filename_final" id="filename_final" class=""/>
                            </div>
                            <div class="form-group">
                                <label for="notes">Catatan <em>(optional)</em></label>
                                <textarea class="form-control" name="editor_notes" id="editor_notes"></textarea>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                
                <input type="hidden" name="document_id" value="{{ $document->id }}"/>
            </div><!-- /.box -->
        </div>
        <div class="col-lg-4 col-md-4"></div>        
    </div>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
</form>