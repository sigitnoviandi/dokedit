@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    Edit Informasi Dokumen
@endsection

@section('contentheader_title')
    Edit Informasi Dokumen
@endsection

@section('contentheader_description')
    Edit informasi mengenai dokumen yang telah Anda kirim
@endsection

@section('page_breadcrumbs')
    {!! Breadcrumbs::render('edit_document', $document->id) !!}
@endsection

@section('custom-css')
<!--<link href="{{ asset('/css/contacts.css') }}" rel="stylesheet" type="text/css" />-->
@endsection

@section('main-content')
<!-- page buttons -->
<div class="row">
    <div class="col-lg-"></div>
</div>
<!-- end of page buttons -->

<!-- alert -->
@if(count(Alert::get()) > 0)        
    @foreach (Alert::get() as $alert)
        <div class="alert alert-{{ $alert->class }} alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p>{{ $alert->message }}</p>
        </div>
    @endforeach                    
@endif
<!-- end of alert -->

<!-- page table -->
@include('document.partials.form_edit')
<!-- end of page table -->
@endsection

@section('custom-js')
<script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('js/document.min.js') }}"></script>
@endsection