@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    @if($logged_user->is_admin())
    Semua Dokumen
    @else
    Dokumen Anda
    @endif
@endsection

@section('contentheader_title')
    @if($logged_user->is_admin())
    Semua Dokumen
    @else
    Dokumen Anda
    @endif
@endsection

@section('contentheader_description')
    @if($logged_user->is_admin())
    Semua dokumen yang diunggah oleh pengguna
    @else
    Semua dokumen yang Anda unggah
    @endif
@endsection

@section('page_breadcrumbs')
    {!! Breadcrumbs::render('manage_documents') !!}
@endsection

@section('custom-css')
<!--<link href="{{ asset('/css/contacts.css') }}" rel="stylesheet" type="text/css" />-->
@endsection

@section('main-content')
    <!-- page buttons -->
    <div class="row button-wrapper">
        <div class="col-lg-6 col-xs-12">
            @if($logged_user->is_author())
            <a href="{{ URL::Route('document.add.get') }}" class="btn btn-primary btn-sm btn-new-document" title="Tambah Dokumen Baru untuk Diedit"><i class="fa fa-plus"></i> Tambah Dokumen</a>
            @endif
        </div>
        <div class="col-lg-6 col-xs-12 action-wrapper">
            <a href="{{ URL::Route('document.filter.clear.get') }}" class="btn btn-default btn-sm btn-reset-filter pull-right" id="btn-clear-filter">Hapus Filter Pencarian</a>
            <button class="btn btn-default btn-sm btn-search-filter pull-right" id="btn-search"><i class="fa fa-search"></i> Cari</button>
        </div>
    </div>
    <!-- end of page buttons -->
    
    <!-- alert -->
    @if(count(Alert::get()) > 0)        
        @foreach (Alert::get() as $alert)
            <div class="alert alert-{{ $alert->class }} alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p>{{ $alert->message }}</p>
            </div>
        @endforeach                    
    @endif
    <!-- end of alert -->

    <!-- page table -->
    @include('document.partials.table')
    <!-- end of page table -->
    
    <div class="modal fade" id="modal-payment-confirmation">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detil Konfirmasi Pembayaran</h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    
    <div class="modal fade" id="modal-editor-notes">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Catatan dari Editor</h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    
@endsection

@section('custom-js')
    <script>
        var AJAX_URL = {
            get_payment_confirmation_detail: "{{ URL::Route('billing.payment_confirmation.detail.post') }}",
            confirm_payment: "{{ URL::Route('billing.payment.confirm.post') }}",
            get_editor_notes: "{{ URL::Route('document.editor_notes.post') }}"
        };
    </script>
    <script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('js/table.min.js') }}"></script>
    <script src="{{ asset('js/document.min.js') }}"></script>
@endsection