@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    {{ trans('app.403_title') }}
@endsection

@section('contentheader_title')
@endsection

@section('contentheader_description')
@endsection

@section('page_breadcrumbs')
@endsection

@section('custom-css')
<!--<link href="{{ asset('/css/contacts.css') }}" rel="stylesheet" type="text/css" />-->
@endsection

@section('main-content')
    <div class="error-page">
        <h2 class="headline text-yellow"> 403</h2>
        <div class="error-content">
            <h3><i class="fa fa-warning text-yellow"></i> {{ trans('app.403_title') }}</h3>
            <p>
                <?php echo trans('app.403_content', ['support_email_link' => '<a href="mailto:' . DE_SUPPORT_EMAIL . '">' . DE_SUPPORT_EMAIL . '</a>']); ?>
            </p>
        </div>
        <br style="clear: both;"/>
        <!-- /.error-content -->
      </div>
@endsection