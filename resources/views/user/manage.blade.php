@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    @if(isset($filter['filter_type']) && $filter['filter_type'] == 'author')
    Daftar Pemilik Dokumen
    @elseif(isset($filter['filter_type']) && $filter['filter_type'] == 'editor')
    Daftar Editor
    @else
    Daftar Pengguna
    @endif
@endsection

@section('contentheader_title')
    @if(isset($filter['filter_type']) && $filter['filter_type'] == 'author')
    Daftar Pemilik Dokumen
    @elseif(isset($filter['filter_type']) && $filter['filter_type'] == 'editor')
    Daftar Editor
    @else
    Daftar Pengguna
    @endif
@endsection

@section('contentheader_description')
    @if(isset($filter['filter_type']) && $filter['filter_type'] == 'author')
    Semua pemilik dokumen yang terdaftar di DokEdit
    @elseif(isset($filter['filter_type']) && $filter['filter_type'] == 'editor')
    Semua editor yang terdaftar di DokEdit
    @else
    Semua pengguna yang terdaftar di DokEdit
    @endif
@endsection

@section('page_breadcrumbs')
    {!! Breadcrumbs::render('manage_admin') !!}
@endsection

@section('custom-css')
<!--<link href="{{ asset('/css/contacts.css') }}" rel="stylesheet" type="text/css" />-->
@endsection

@section('main-content')
    <!-- page buttons -->
    <div class="row button-wrapper">
        <div class="col-lg-6 col-xs-12">
            <a href="{{ URL::Route('admin.add.get') }}" class="btn btn-default btn-sm btn-new-admin" title="Add a New Admin"><i class="fa fa-plus"></i> Tambah Pengguna Baru</a>
        </div>
        <div class="col-lg-6 col-xs-12 action-wrapper">
            <a href="{{ URL::Route('admin.filter.clear.get') }}" class="btn btn-default btn-sm btn-reset-filter pull-right" id="btn-clear-filter">Hapus Filter Pencarian</a>
            <button class="btn btn-default btn-sm btn-search-filter pull-right" id="btn-search"><i class="fa fa-search"></i> Cari</button>
        </div>
    </div>
    <!-- end of page buttons -->
    
    <!-- alert -->
    @if(count(Alert::get()) > 0)        
        @foreach (Alert::get() as $alert)
            <div class="alert alert-{{ $alert->class }} alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p>{{ $alert->message }}</p>
            </div>
        @endforeach                    
    @endif
    <!-- end of alert -->

    <!-- page table -->
    @include('user.partials.table')
    <!-- end of page table -->
    
@endsection

@section('custom-js')
    <script>
        var AJAX_URL = {
            activate_user: "{{ URL::Route('admin.activate_user.post') }}",
            resend_activation: "{{ URL::Route('admin.resend_activation.post') }}"
        };
    </script>
    <script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('js/table.min.js') }}"></script>
    <script src="{{ asset('js/user.min.js') }}"></script>
@endsection