@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
Dasbor
@endsection

@section('contentheader_title')
Selamat Datang!
@endsection

@section('contentheader_description')
DokEdit
@endsection


@section('main-content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">Home</div>

                <div class="panel-body">
                    Selamat datang di Dokedit!
                </div>
            </div>
        </div>
    </div>
    @if($logged_user->is_admin())
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{ $quick_stats['total_documents'] }}</h3>
                    <p>Total Dokumen Diupload</p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios-folder"></i>
                </div>            
                <a href="{{ URL::Route('document.manage.get') }}" class="small-box-footer">Daftar Dokumen <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{ $quick_stats['total_users'] }}</h3>
                    <p>Total Pengguna</p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios-people"></i>
                </div>                
                <a href="{{ URL::Route('admin.manage.get') }}" class="small-box-footer">Daftar Pengguna <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{ $quick_stats['total_documents_uploaded_today'] }}</h3>
                    <p>Dokumen diunggah hari ini</p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios-document"></i>
                </div>            
                <a href="{{ URL::Route('document.manage.get') }}" class="small-box-footer">Daftar Dokumen <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>{{ $quick_stats['total_documents_completed_today'] }}</h3>
                    <p>Dokumen selesai hari ini</p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios-checkmark-circle"></i>
                </div>                
                <a href="{{ URL::Route('document.manage.get') }}" class="small-box-footer">Daftar Dokumen <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>

    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Jenis Dokumen</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="chart-responsive">
                                <canvas id="documents_pie_chart" height="155" width="328" style="width: 328px; height: 155px;"></canvas>
                            </div>
                            <!-- ./chart-responsive -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer no-padding">
                    <ul class="nav nav-pills nav-stacked">
                        <li>
                            <a>
                                <i class="fa fa-circle-o text-aqua"></i> Naskah/Teks
                                <span class="pull-right text-aqua">{{ number_format($quick_stats['total_texts_percentage']) . '%' }}</span>
                            </a>
                        </li>
                        <li>
                            <a>
                                <i class="fa fa-circle-o text-yellow"></i> Grafis/Slide
                                <span class="pull-right text-yellow">{{ number_format($quick_stats['total_graphics_percentage']) . '%' }}</span>
                            </a>
                        </li>
                        
                    </ul>
                </div>
                <!-- /.footer -->
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Pengguna</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="chart-responsive">
                                <canvas id="users_pie_chart" height="155" width="328" style="width: 328px; height: 155px;"></canvas>
                            </div>
                            <!-- ./chart-responsive -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer no-padding">
                    <ul class="nav nav-pills nav-stacked">
                        <li>
                            <a>
                                <i class="fa fa-circle-o text-green"></i> Pemilik Dokumen
                                <span class="pull-right text-green">{{ number_format($quick_stats['total_authors_percentage']) . '%' }}</span>
                            </a>
                        </li>
                        <li>
                            <a>
                                <i class="fa fa-circle-o text-red"></i> Editor
                                <span class="pull-right text-red">{{ number_format($quick_stats['total_editors_percentage']) . '%' }}</span>
                            </a>
                        </li>
                        
                    </ul>
                </div>
                <!-- /.footer -->
            </div>
        </div>
        <!-- ./col -->        
        <div class="col-lg-3 col-xs-6">
            
        </div>
        <div class="col-lg-3 col-xs-6">
            
        </div>
        <!-- ./col -->
    </div>
    @endif
</div>
@endsection

@section('custom-js')
<script type="text/javascript" src="{{ asset('plugins/chartjs/Chart.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/user.min.js') }}"></script>
@if($logged_user->is_admin())
<script>
var documents_pie_data = [{
    value: {{ $quick_stats['total_texts'] }},
    color: '#00C0EF',
    highlight: '#4cdeff',
    label: 'Teks/Naskah'
}, {
    value: {{ $quick_stats['total_graphics'] }},
    color: '#F39C12',
    highlight: '#f4ab3d',
    label: 'Grafis/Slide'
}];
var users_pie_data = [{
    value: {{ $quick_stats['total_authors'] }},
    color: '#00A65A',
    highlight: '#04cc6e',
    label: 'Pemilik Dokumen'
}, {
    value: {{ $quick_stats['total_editors'] }},
    color: '#F56954',
    highlight: '#f47b6b',
    label: 'Editor'
}];

User.Dashboard.init();
</script>
@endif
@endsection
