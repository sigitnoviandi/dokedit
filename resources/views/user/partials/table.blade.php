<div class="box table-container">
    <div class="box-header">
        Tampilkan 
        <select name="limit" id="filter-limit-options" class="limit-options">
            <option value="10" @if(isset($filter['filter_limit']) && $filter['filter_limit'] == 10) selected @endif>10</option>
            <option value="20" @if(isset($filter['filter_limit']) && $filter['filter_limit'] == 20) selected @endif>20</option>
            <option value="40" @if(isset($filter['filter_limit']) && $filter['filter_limit'] == 40) selected @endif>40</option>
            <option value="50" @if(isset($filter['filter_limit']) && $filter['filter_limit'] == 50) selected @endif>50</option>
            <option value="100" @if(isset($filter['filter_limit']) && $filter['filter_limit'] == 100) selected @endif>100</option>
        </select> baris
        <div class="box-tools">
            @include('pagination.default', ['paginator' => $users])
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding">
        <form id="filter-form" method="post" action="{{ URL::Route('admin.filter.set.post') }}">            
            <table class="table table-striped table-hover table-bordered">
                <tr>
                    <th style="width: 2%;">#</th>
                    <th style="width: 15%;">Name</th>
                    <th style="width: 25%;">Email</th>
                    <th style="width: 10%;">Role</th>
                    <th style="width: 10%;">Type</th>
                    <th style="width: 10%;">Status</th>
                    <th style="width: 10%;">Last Login</th>
                    <th style="width: 10%;">&nbsp;</th>
                </tr>
                <tr class="filter">
                    <td>&nbsp;</td>
                    <td><input type="text" name="filter_name" class="form-control input-sm" placeholder="Search Name" value="{{ isset($filter['filter_name']) ? $filter['filter_name'] : '' }}"/></td>
                    <td><input type="text" name="filter_email" class="form-control input-sm" placeholder="Search Email" value="{{ isset($filter['filter_email']) ? $filter['filter_email'] : '' }}"/></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                @if(isset($users) && !empty($users))
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $row_no++ }}</td>
                            <td>{{ $user->name() }}</td>
                            <td>{{ $user->email }}</td>
                            <td>
                                @foreach($user->roles as $role)
                                    {{ $role->name }}
                                @endforeach
                            </td>
                            <td>
                                {{ $user->type }}
                            </td>
                            <td>
                                @if($user->status == 0)
                                <a class="btn-activate-user" title="Activate this User" data-user-id="{{ $user->id }}" data-user-name="{{ $user->name() }}">
                                    <span class="label label-warning">Not Activated</span>
                                </a>
                                <a class="btn btn-default btn-xs btn-resend-activation btn-new-line" title="Resend Activation Email" data-user-id="{{ $user->id }}" data-user-name="{{ $user->name() }}"><i class="fa fa-envelope"></i> Resend Activation</a>
                                @elseif($user->status == 1)
                                <span class="label label-success">Active</span>
                                @endif
                            </td>
                            <td>
                                @if(isset($user->last_login) && !empty($user->last_login))
                                {{ \Carbon\Carbon::parse($user->last_login)->diffForHumans() }}
                                @endif
                            </td>
                            <td class="action-buttons">
                                <a href="{{ URL::Route('admin.remove.get', [$user->id]) }}" class="btn btn-danger btn-sm btn-remove" data-record-name="{{ $user->first_name . ' ' . $user->last_name }}"><i class="fa fa-trash"></i></a>
                                <a href="{{ URL::Route('admin.edit.get', [$user->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a>
                            </td>
                        </tr>
                    @endforeach
                @else
                <tr>
                    <td colspan="6">Data tidak ditemukan.</td>
                </tr>
                @endif
            </table>
            <input type="hidden" value="10" name="filter_limit" id="filter-limit"/>
            <input type="hidden" value="{{ isset($filter['filter_type']) ? $filter['filter_type'] : '' }}" name="filter_type" id="filter-type"/>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>
    </div>
    <!-- /.box-body -->
    <div class="box-footer clearfix">
        <span class="no-of-records">Menampilkan baris {{ $first_record_no }}&ndash;{{ $row_no - 1 }} dari total {{ number_format($users->total()) }} baris.</span>
        @include('pagination.default', ['paginator' => $users])
    </div>
</div>
<!-- /.box -->