<!-- general form elements -->
<!-- form start -->
<form role="form" id="form_new_user" action="{{ URL::Route('admin.save.post') }}" method="POST">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-default">
                <div class="box-body action-wrapper">  
                    <span>Your changes won't be save until you hit the Save button.</span>
                    <a class="btn btn-danger pull-right" id="btn_reset">Reset</a>
                    <a class="btn btn-default pull-right" href="{{ URL::Route('admin.manage.get') }}" id="btn_cancel">Cancel</a>
                    <a class="btn btn-primary pull-right" id="btn_save">Save</a>
                    <span id="loading-indicator" class="loading-indicator pull-right inactive"><i class="fa fa-spinner fa-spin fa-pulse fa-lg"></i></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Admin Info</h3>
                </div><!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <div class="form-group">
                        <label for="first_name">Nama Depan *</label>
                        <input type="text" name="first_name" class="form-control" id="first_name" placeholder="First Name" value="{{ isset($user->first_name) ? $user->first_name : '' }}">
                    </div>
                    <div class="form-group">
                        <label for="last_name">Nama Belakang *</label>
                        <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Last Name" value="{{ isset($user->last_name) ? $user->last_name : '' }}">
                    </div>
                    <div class="form-group">
                        <label for="email">Alamat Email *</label>
                        <input type="email" name="email" class="form-control" id="email" placeholder="Email Address" value="{{ isset($user->email) ? $user->email : '' }}">
                    </div>
                    <div class="form-group">
                        <label for="username">Username *</label>
                        <input type="username" name="username" class="form-control" id="username" placeholder="Username" value="{{ isset($user->username) ? $user->username : '' }}" disabled>
                    </div>
                    <div class="form-group">
                        <label for="mobile">No HP *</label>
                        <input type="text" name="mobile" class="form-control" id="mobile" placeholder="No. Handphone" value="{{ isset($user->mobile) ? $user->mobile : '' }}">
                    </div>
                    <div class="form-group">
                        <label for="role">Role</label>                        
                        <select class="form-control" name="role" id="role">
                            @foreach($roles as $role)
                                @if(isset($user) && !empty($user) && isset($user->id) && !empty($user->id))
                                    <option value="{{ $role->id }}" @if($user->inRole($role->slug)) selected @endif>{{ $role->name }}</option>
                                @else
                                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="password">Kata Sandi/Password</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                        @if(isset($password))
                        <p>Password: <em>{{ $password }}</em></p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="confirm_password">Konfirmasi Kata Sandi/Password</label>
                        <input type="password" name="confirm_password" class="form-control" id="confirm_password" placeholder="Confirm Password">
                    </div>
                    @if(isset($user) && !empty($user))
                        <div class="form-group">
                            <label for="organization">Organisasi/Perusahaan *</label>
                            <input type="text" name="organization" class="form-control" id="organization" placeholder="Organisasi/Perusahaan" value="{{ isset($user->organization) ? $user->organization : '' }}">
                        </div>
                        <div class="form-group">
                            <label for="type">Tipe Pengguna</label>
                            <input type="text" name="type" class="form-control" id="type" placeholder="Tipe Pengguna" value="{{ ucfirst($user->type) }}" disabled>
                        </div>
                        @if($user->type == 'editor')
                            <div class="form-group">
                                <label for="type">Keahlian</label>
                                <input type="text" name="skill" class="form-control" id="skill" placeholder="Keahlian" value="{{ ucfirst($user->skill) }}" disabled>
                            </div>
                            @if($user->skill == 'substansi')
                            <div class="form-group">
                                <label for="type">Substansi Keahlian</label><br/>
                                <?php
                                    $subskills = unserialize($user->subskill);
                                ?>
                                
                                @if(!empty($subskills))
                                    @foreach($subskills as $subskill)
                                        <span class="label label-primary label-lg">{{ $subskill }}</span>
                                    @endforeach
                                @endif
                            </div>
                            @endif
                        @endif
                    @endif
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="user_id" name="user_id" value="{{ isset($user->id) && !empty($user->id) ? $user->id : ''}}">
</form>
