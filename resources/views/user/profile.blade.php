@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    {{ trans('users/profile.page_title', ['name' => $user->name()]) }}
@endsection

@section('contentheader_title')
    {{ trans('users/profile.page_heading', ['name' => $user->name()]) }}
@endsection

@section('contentheader_description')
    {{ trans('users/profile.page_subheading', ['name' => $user->name()]) }}
@endsection

@section('page_breadcrumbs')
    {!! Breadcrumbs::render('view_user_profile', $user->id, $user->name()) !!}
@endsection

@section('custom-css')
<link href="{{ asset('/css/user-profile.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('main-content')
    
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Error!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    
    <!-- alert -->
    @if(count(Alert::get()) > 0)        
        @foreach (Alert::get() as $alert)
            <div class="alert alert-{{ $alert->class }} alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p>{!! $alert->message !!}</p>
            </div>
        @endforeach                    
    @endif
    <!-- end of alert -->
    
<div class="row user-profile">
    <div class="col-md-3">

        <!-- Profile Image -->
        <div class="box box-primary">
            <div class="box-body box-profile">
                <img class="profile-user-img img-responsive img-circle" src="{{ $user->avatar() }}" alt="User profile picture">
                <h3 class="profile-username text-center">{{ $user->name() }}</h3>
                <p class="text-muted text-center">
                    
                </p>
                <p class="text-center">
                    @if($user_id == 'my')
                        @if($logged_user->hasAccess(['user.profile.edit.my']))
                        <a id="btn-show-edit-form" class="text-center">{{ trans('users/profile.edit_my_profile_link_text') }}</a>
                        @endif
                    @else
                        @if($logged_user->hasAccess(['user.profile.edit.other']))
                        <a id="btn-show-edit-form" class="text-center">{{ trans('users/profile.edit_profile_link_text') }}</a>
                        @endif
                    @endif
                </p>
                
                <p class="text-center">
                    <b>{{ trans('users/profile.last_login') }}</b> {{ $user->last_login() }}
                </p>

                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b>{{ trans('users/profile.username') }}</b> <span class="pull-right">{{ $user->username }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>{{ trans('users/profile.email') }}</b> <a class="pull-right" href="mailto:{{ $user->email }}">{{ $user->email }}</a>
                    </li>
                    <li class="list-group-item">
                        <b>{{ trans('users/profile.mobile') }}</b> <span class="pull-right">{{ $user->mobile }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>{{ trans('users/profile.user_type') }}</b> <span class="pull-right">{{ ucfirst($user->type) }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>{{ trans('users/profile.organization') }}</b> <span class="pull-right">{{ $user->organization }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>{{ trans('users/profile.skill') }}</b> <span class="pull-right">{{ ucfirst($user->skill) }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>{{ trans('users/profile.subskill') }}</b><br/>{!! $user->subskill(TRUE) !!}
                    </li>
                </ul>
            </div><!-- /.box-body -->
        </div><!-- /.box -->

        <!-- About Me Box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('users/profile.about_me') }}</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <!--<strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>-->
                <p class="text-muted">{{ $user->about_me() }}</p>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
    
    <div class="col-md-9">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#reviews" data-toggle="tab">{{ trans('users/profile.reviews') }}</a></li>
                <li id="user-profile-forms-tab"><a href="#user-profile-forms" data-toggle="tab">{{ trans('users/profile.edit') }}</a></li>
            </ul>
            <div class="tab-content">
                <div class="active tab-pane fade in" id="reviews">
                    <!-- page buttons -->
                    <div class="row button-wrapper">
                        <div class="col-lg-12 col-xs-12">
                            <h4>{{ trans('users/profile.reviews') }}</h4>
                            <br/>
                            <div class="post">
                                <div class="user-block">
                                    <img class="img-circle img-bordered-sm" src="{{ asset('img/user1-128x128.jpg') }}" alt="user image">
                                    <span class="username">
                                        <a href="#">Jonathan Burke Jr.</a>
                                    </span>
                                    <span class="description">Shared publicly - 7:30 PM today</span>
                                </div>
                                <!-- /.user-block -->
                                <p>
                                    Lorem ipsum represents a long-held tradition for designers,
                                    typographers and the like. Some people hate it and argue for
                                    its demise, but others ignore the hate as they create awesome
                                    tools to help create filler text for everyone from bacon lovers
                                    to Charlie Sheen fans.
                                </p>
                            </div>
                            <div class="post">
                                <div class="user-block">
                                    <img class="img-circle img-bordered-sm" src="{{ asset('img/user4-128x128.jpg') }}" alt="user image">
                                    <span class="username">
                                        <a href="#">Megan Smith</a>
                                    </span>
                                    <span class="description">Shared publicly - 7:30 PM today</span>
                                </div>
                                <!-- /.user-block -->
                                <p>
                                    Lorem ipsum represents a long-held tradition for designers,
                                    typographers and the like. Some people hate it and argue for
                                    its demise, but others ignore the hate as they create awesome
                                    tools to help create filler text for everyone from bacon lovers
                                    to Charlie Sheen fans.
                                </p>
                            </div>
                            <div class="post">
                                <div class="user-block">
                                    <img class="img-circle img-bordered-sm" src="{{ asset('img/user3-128x128.jpg') }}" alt="user image">
                                    <span class="username">
                                        <a href="#">Alisha Key</a>
                                    </span>
                                    <span class="description">Shared publicly - 7:30 PM today</span>
                                </div>
                                <!-- /.user-block -->
                                <p>
                                    Lorem ipsum represents a long-held tradition for designers,
                                    typographers and the like. Some people hate it and argue for
                                    its demise, but others ignore the hate as they create awesome
                                    tools to help create filler text for everyone from bacon lovers
                                    to Charlie Sheen fans.
                                </p>
                            </div>
                            <div class="post">
                                <div class="user-block">
                                    <img class="img-circle img-bordered-sm" src="{{ asset('img/user5-128x128.jpg') }}" alt="user image">
                                    <span class="username">
                                        <a href="#">Jane Doe</a>
                                    </span>
                                    <span class="description">Shared publicly - 7:30 PM today</span>
                                </div>
                                <!-- /.user-block -->
                                <p>
                                    Lorem ipsum represents a long-held tradition for designers,
                                    typographers and the like. Some people hate it and argue for
                                    its demise, but others ignore the hate as they create awesome
                                    tools to help create filler text for everyone from bacon lovers
                                    to Charlie Sheen fans.
                                </p>
                            </div>
                        </div>
                    </div>
                    <br style="clear:both;"/>
                </div><!-- /.tab-pane -->
                <div class="tab-pane fade in" id="user-profile-forms">
                    <form role="form" class="form-horizontal" id="form_edit_profile" action="{{ URL::Route('user.profile.save.post', [$user_id]) }}" method="POST" enctype="multipart/form-data">
                        <fieldset>
                            <legend><h4>Informasi Akun</h4></legend>
                            <div class="form-group">
                                <label for="username" class="col-sm-2 control-label">Username *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="username" placeholder="Username" value="{{ $user->username }}" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-sm-2 control-label">Email *</label>

                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Alamat Email" value="{{ $user->email }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password" class="col-sm-2 control-label">Password</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="password" name="password" placeholder="Kata Sandi/Password" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password_confirmation" class="col-sm-2 control-label">Konfirmasi Kata Sandi/Password</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Konfirmasi Password" value="">
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend><h4>Data Pribadi</h4></legend>
                            <div class="form-group">
                                <label for="first_name" class="col-sm-2 control-label">Nama Depan *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Nama Depan" value="{{ $user->first_name }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="last_name" class="col-sm-2 control-label">Nama Belakang *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Nama Belakang" value="{{ $user->last_name }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="mobile" class="col-sm-2 control-label">No. Handphone *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="mobile" name="mobile" placeholder="No. Handphone" value="{{ $user->mobile }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="organization" class="col-sm-2 control-label">Organisasi/Perusahaan</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="organization" name="organization" placeholder="Organisasi/Perusahaan" value="{{ $user->organization }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="avatar" class="col-sm-2 control-label">Upload Foto Profil</label>
                                <div class="col-sm-10">
                                    <input type="file" class="" id="avatar" name="avatar" placeholder="">
                                    <span class="help-block">Rekomendasi dimensi: 500px x 500px, dalam format .jpeg .jpg .png dengan ukuran file kurang dari 2MB</span>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend><h4>Biodata Singkat</h4></legend>
                            <div class="form-group">
                                <label for="organization" class="col-sm-2 control-label">Biodata Anda</label>
                                <div class="col-sm-10">                                    
                                    <textarea type="text" class="form-control" id="description" name="description" rows="6">{{ $user->description }}</textarea>
                                    <span class="help-block">Tuliskan sedikit informasi tentang diri Anda.</span>
                                </div>
                            </div>
                        </fieldset>
                        @if($user->is_editor() == TRUE)
                        <fieldset>
                            <legend><h4>Data Editor</h4></legend>
                            <div class="form-group">
                                <label for="skill" class="col-sm-2 control-label">Keahlian</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="skill" id="skill">
                                        <option value="ejaan" <?php if($user->skill == 'ejaan') echo 'selected'; ?>>{{ trans('adminlte_lang::message.ejaan') }}</option>
                                        <option value="tatabahasa" <?php if($user->skill == 'tatabahasa') echo 'selected'; ?>>{{ trans('adminlte_lang::message.tatabahasa') }}</option>
                                        <option value="substansi" <?php if($user->skill == 'substansi') echo 'selected'; ?>>{{ trans('adminlte_lang::message.substansi') }}</option>
                                    </select>
                                </div>
                            </div>
                            @if($user->skill == 'substansi')
                            <div class="form-group" id="subskills-wrapper">
                                <label for="subskill" class="col-sm-2 control-label">Substansi Keahlian</label>
                                <div class="col-sm-10">
                                    <?php
                                    $subskills = $user->subskill(FALSE);
                                    if(empty($subskills)){
                                        $subskills = [];
                                    }
                                    ?>
                                    <label><div class="icheck"><label><input type="checkbox" name="subskill[]" value="{{ trans('adminlte_lang::message.sains') }}" <?php if(in_array(trans('adminlte_lang::message.sains'), $subskills)) echo 'checked'; ?>> {{ trans('adminlte_lang::message.sains') }}</label></div></label><br/>
                                    <label><div class="icheck"><label><input type="checkbox" name="subskill[]" value="{{ trans('adminlte_lang::message.social_humaniora') }}" <?php if(in_array(trans('adminlte_lang::message.social_humaniora'), $subskills)) echo 'checked'; ?>> {{ trans('adminlte_lang::message.social_humaniora') }}</label></div></label><br/>
                                    <label><div class="icheck"><label><input type="checkbox" name="subskill[]" value="{{ trans('adminlte_lang::message.teknologi_teknik') }}" <?php if(in_array(trans('adminlte_lang::message.teknologi_teknik'), $subskills)) echo 'checked'; ?>> {{ trans('adminlte_lang::message.teknologi_teknik') }}</label></div></label><br/>
                                    <label><div class="icheck"><label><input type="checkbox" name="subskill[]" value="{{ trans('adminlte_lang::message.sastra_budaya') }}" <?php if(in_array(trans('adminlte_lang::message.sastra_budaya'), $subskills)) echo 'checked'; ?>> {{ trans('adminlte_lang::message.sastra_budaya') }}</label></div></label><br/>
                                    <label><div class="icheck"><label><input type="checkbox" name="subskill[]" value="{{ trans('adminlte_lang::message.ekonomi_bisnis') }}" <?php if(in_array(trans('adminlte_lang::message.ekonomi_bisnis'), $subskills)) echo 'checked'; ?>> {{ trans('adminlte_lang::message.ekonomi_bisnis') }}</label></div></label><br/>
                                    <label><div class="icheck"><label><input type="checkbox" name="subskill[]" value="{{ trans('adminlte_lang::message.agama') }}" <?php if(in_array(trans('adminlte_lang::message.agama'), $subskills)) echo 'checked'; ?>> {{ trans('adminlte_lang::message.agama') }}</label></div></label><br/>
                                    <label><div class="icheck"><label><input type="checkbox" name="subskill[]" value="{{ trans('adminlte_lang::message.bahasa_asing') }}" <?php if(in_array(trans('adminlte_lang::message.bahasa_asing'), $subskills)) echo 'checked'; ?>> {{ trans('adminlte_lang::message.bahasa_asing') }}</label></div></label><br/>
                                    <?php
                                    $default_subskills = App\Models\User::default_subskills();
                                    $other_subskills = array_diff($subskills, $default_subskills);
                                    $other_subskills = array_values($other_subskills);
                                    ?>
                                    <input type="text" class="form-control other-subskill-sample other-subskill" placeholder="{{ trans('adminlte_lang::message.other_subskill') }}" name="other_subskill[]" value="<?php if(!empty($other_subskills)) echo $other_subskills[0]; ?>"/>
                                    <?php
                                    array_shift($other_subskills);
                                    ?>
                                    @if(count($other_subskills) > 1)
                                        @foreach($other_subskills as $other_subskill)
                                            @if(!empty($other_subskill))
                                                <input type="text" class="form-control other-subskill" placeholder="{{ trans('adminlte_lang::message.other_subskill') }}" name="other_subskill[]" value="<?php echo $other_subskill; ?>"/>
                                            @endif
                                        @endforeach
                                    @endif
                                    <a id="add-other-subskill">{{ trans('adminlte_lang::message.add_subskill') }}</a>
                                </div>
                            </div>
                            @endif
                        </fieldset>
                        @endif
                        <div class="form-group buttons-wrapper">
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="user_id" value="{{ $user->id }}"/>
                                <button type="reset" class="btn btn-default">Reset</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.tab-pane -->
            </div><!-- /.tab-content -->
        </div><!-- /.nav-tabs-custom -->
    </div><!-- /.col -->
</div><!-- /.row -->
@endsection

@section('custom-js')
<script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('js/user.min.js') }}"></script>
@endsection