@extends('adminlte::layouts.auth')

@section('htmlheader_title')
    Ubah Password
@endsection

@section('content')

    <body class="login-page">

    <div id="app">
        <div class="login-box">
        <div class="login-logo">
            <a href="{{ url('/home') }}"><b>Dok</b>Edit</a>
        </div><!-- /.login-logo -->

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
        <!-- alert -->
        @if(count(Alert::get()) > 0)        
            @foreach (Alert::get() as $alert)
                <div class="alert alert-{{ $alert->class }} alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p>{!! $alert->message !!}</p>
                </div>
            @endforeach                    
        @endif
        <!-- end of alert -->
        
        <div class="login-box-body">
            <p class="login-box-msg">{{ trans('adminlte_lang::message.passwordreset') }}</p>
            <form action="{{ URL::Route('auth.reset_password') }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="token" value="{{ $token }}">
                <input type="hidden" name="original_token" value="{{ $original_token }}">
                <!--<input type="hidden" value="{{ $user_id }}" name="user_id"/>-->
                <input type="hidden" value="{{ $user_id }}" name="user_id"/>
                
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Kata Sandi/Password" name="password"/>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>

                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Konfirmasi Kata Sandi/Password" name="password_confirmation"/>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">{{ trans('adminlte_lang::message.passwordreset') }}</button>
                    </div><!-- /.col -->
                </div>
                
            </form>

            <div class="text-center" style="margin-top: 20px;">
                <a href="{{ url('/login') }}" class="text-center">{{ trans('adminlte_lang::message.membership') }}</a><br/>
                {{ trans('adminlte_lang::message.notyetamember') }} <a href="{{ url('/register') }}" class="text-center"><b>{{ trans('adminlte_lang::message.registermember') }}</b></a>
            </div>
        </div><!-- /.login-box-body -->

    </div><!-- /.login-box -->
    </div>

    @include('adminlte::layouts.partials.scripts_auth')

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
    </body>

@endsection
