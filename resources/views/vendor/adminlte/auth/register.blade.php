@extends('adminlte::layouts.auth')

@section('htmlheader_title')
    Register
@endsection

@section('custom-css')
<link href="{{ asset('/css/auth/register.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<body class="hold-transition register-page">
    <div id="app">
        <div class="register-box">
            <div class="register-logo">
                <a href="{{ url('/home') }}"><b>Dok</b>Edit</a>
            </div>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Error!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            
            <!-- alert -->
            @if(count(Alert::get()) > 0)        
                @foreach (Alert::get() as $alert)
                    <div class="alert alert-{{ $alert->class }} alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <p>{{ $alert->message }}</p>
                    </div>
                @endforeach                    
            @endif
            <!-- end of alert -->

            <div class="register-box-body">
                <h3 class="login-box-msg">{{ trans('adminlte_lang::message.registermember') }}</h3>
                <form action="{{ URL::Route('auth.register') }}" method="post" id="registration-form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <h5>{{ trans('adminlte_lang::message.register_section_account') }}</h5>
                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" placeholder="{{ trans('adminlte_lang::message.email') . ' *' }}" name="email" value="{{ old('email') }}"/>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="{{ trans('adminlte_lang::message.username') . ' *' }}" name="username" value="{{ old('username') }}"/>
                        <span class="glyphicon glyphicon-heart form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="{{ trans('adminlte_lang::message.password') . ' *' }}" name="password"/>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="{{ trans('adminlte_lang::message.retrypepassword') . ' *' }}" name="password_confirmation"/>
                        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                    </div>
                    
                    <h5>{{ trans('adminlte_lang::message.register_section_general') }}</h5>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="{{ trans('adminlte_lang::message.firstname') . ' *' }}" name="firstname" value="{{ old('firstname') }}"/>
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="{{ trans('adminlte_lang::message.lastname') . ' *' }}" name="lastname" value="{{ old('lastname') }}"/>
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="{{ trans('adminlte_lang::message.mobile') . ' *' }}" name="mobile" value="{{ old('mobile') }}"/>
                        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="{{ trans('adminlte_lang::message.organization') }}" name="organization" value="{{ old('organization') }}"/>
                        <span class="glyphicon glyphicon-briefcase form-control-feedback"></span>
                    </div>
                    
                    <h5>{{ trans('adminlte_lang::message.register_section_registeras') }}</h5>
                    <div class="form-group has-feedback">
                        <select class="form-control" name="type" id="type">
                            <option value="author" <?php if(old('type') == 'author') echo 'selected'; ?>>Penulis/Pemilik Dokumen</option>
                            <option value="editor" <?php if(old('type') == 'editor') echo 'selected'; ?>>Editor</option>
                        </select>
                    </div>
                    
                    <div class="editor-data">
                        <h5>{{ trans('adminlte_lang::message.register_section_editor_data') }}</h5>
                        <div class="form-group has-feedback">
                            <label>{{ trans('adminlte_lang::message.skill') }}: </label>
                            <select class="form-control" name="skill" id="skill">
                                <option value="ejaan" <?php if(old('skill') == 'ejaan') echo 'selected'; ?>>{{ trans('adminlte_lang::message.ejaan') }}</option>
                                <option value="tatabahasa" <?php if(old('skill') == 'tatabahasa') echo 'selected'; ?>>{{ trans('adminlte_lang::message.tatabahasa') }}</option>
                                <option value="substansi" <?php if(old('skill') == 'substansi') echo 'selected'; ?>>{{ trans('adminlte_lang::message.substansi') }}</option>
                            </select>
                        </div>
                        <div class="form-group has-feedback" id="subskills-wrapper">
                            <label>{{ trans('adminlte_lang::message.subskill') }}: </label><br/>
                            <?php
                            $old_subskills = old('subskill');
                            if(empty($old_subskills)){
                                $old_subskills = [];
                            }
                            ?>
                            <label><div class="icheck"><label><input type="checkbox" name="subskill[]" value="{{ trans('adminlte_lang::message.sains') }}" <?php if(in_array(trans('adminlte_lang::message.sains'), $old_subskills)) echo 'checked'; ?>> {{ trans('adminlte_lang::message.sains') }}</label></div></label><br/>
                            <label><div class="icheck"><label><input type="checkbox" name="subskill[]" value="{{ trans('adminlte_lang::message.social_humaniora') }}" <?php if(in_array(trans('adminlte_lang::message.social_humaniora'), $old_subskills)) echo 'checked'; ?>> {{ trans('adminlte_lang::message.social_humaniora') }}</label></div></label><br/>
                            <label><div class="icheck"><label><input type="checkbox" name="subskill[]" value="{{ trans('adminlte_lang::message.teknologi_teknik') }}" <?php if(in_array(trans('adminlte_lang::message.teknologi_teknik'), $old_subskills)) echo 'checked'; ?>> {{ trans('adminlte_lang::message.teknologi_teknik') }}</label></div></label><br/>
                            <label><div class="icheck"><label><input type="checkbox" name="subskill[]" value="{{ trans('adminlte_lang::message.sastra_budaya') }}" <?php if(in_array(trans('adminlte_lang::message.sastra_budaya'), $old_subskills)) echo 'checked'; ?>> {{ trans('adminlte_lang::message.sastra_budaya') }}</label></div></label><br/>
                            <label><div class="icheck"><label><input type="checkbox" name="subskill[]" value="{{ trans('adminlte_lang::message.ekonomi_bisnis') }}" <?php if(in_array(trans('adminlte_lang::message.ekonomi_bisnis'), $old_subskills)) echo 'checked'; ?>> {{ trans('adminlte_lang::message.ekonomi_bisnis') }}</label></div></label><br/>
                            <label><div class="icheck"><label><input type="checkbox" name="subskill[]" value="{{ trans('adminlte_lang::message.agama') }}" <?php if(in_array(trans('adminlte_lang::message.agama'), $old_subskills)) echo 'checked'; ?>> {{ trans('adminlte_lang::message.agama') }}</label></div></label><br/>
                            <label><div class="icheck"><label><input type="checkbox" name="subskill[]" value="{{ trans('adminlte_lang::message.bahasa_asing') }}" <?php if(in_array(trans('adminlte_lang::message.bahasa_asing'), $old_subskills)) echo 'checked'; ?>> {{ trans('adminlte_lang::message.bahasa_asing') }}</label></div></label><br/>
                            <?php
                            $old_other_subskills = old('other_subskill');
                            if(empty($old_other_subskills)){
                                $old_other_subskills = [];
                            }
                            ?>
                            <input type="text" class="form-control other-subskill-sample other-subskill" placeholder="{{ trans('adminlte_lang::message.other_subskill') }}" name="other_subskill[]" value="<?php if(!empty($old_other_subskills)) echo $old_other_subskills[0]; ?>"/>
                            <?php
                            array_shift($old_other_subskills);
                            ?>
                            @if(count($old_other_subskills) > 1)
                                @foreach($old_other_subskills as $old_other_subskill)
                                    @if(!empty($old_other_subskill))
                                        <input type="text" class="form-control other-subskill" placeholder="{{ trans('adminlte_lang::message.other_subskill') }}" name="other_subskill[]" value="<?php echo $old_other_subskill; ?>"/>
                                    @endif
                                @endforeach
                            @endif
                            <a id="add-other-subskill">{{ trans('adminlte_lang::message.add_subskill') }}</a>
                        </div>
                    </div>
                    
                    <div class="editor-data">
                        <h5>{{ trans('adminlte_lang::message.register_section_editor_terms') }}</h5>
                        <label><div class="icheck"><label><input type="checkbox" name="terms_agree_editor" value="1"> <a href="#" data-toggle="modal" data-target="#termsEditorModal">{{ trans('adminlte_lang::message.terms_editor_agree') }}</a></label></div></label><br/>
                    </div>
                    
                    <div class="author-data">
                        <h5>{{ trans('adminlte_lang::message.register_section_terms') }}</h5>
                        <label><div class="icheck"><label><input type="checkbox" name="terms_agree" value="1"> <a href="#" data-toggle="modal" data-target="#termsModal">{{ trans('adminlte_lang::message.terms_agree') }}</a></label></div></label><br/>
                    </div>
                    
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">{{ trans('adminlte_lang::message.register') }}</button>
                        </div><!-- /.col -->
                    </div>
                    
                </form>
                <br/>
                @include('adminlte::auth.partials.social_login')

                <div class="text-center">
                    <a href="{{ url('/login') }}" class="text-center">{{ trans('adminlte_lang::message.membership') }}</a>
                </div>
            </div><!-- /.form-box -->
        </div><!-- /.register-box -->
    </div>

    @include('adminlte::layouts.partials.scripts_auth')

    @include('adminlte::auth.terms')
    
    @include('adminlte::auth.terms_editor')
    <script src="{{ asset('/js/validation.min.js') }}"></script>
    <script src="{{ asset('/js/auth/register.min.js') }}"></script>
</body>

@endsection
