<!-- Terms and conditions modal -->
<div class="modal fade" id="termsEditorModal" tabindex="-1" role="dialog" aria-labelledby="Terms and conditions" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ trans('adminlte_lang::message.terms_editor_title') }}</h3>
            </div>

            <div class="modal-body">
                {!! trans('adminlte_lang::message.terms_editor_content') !!}
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>