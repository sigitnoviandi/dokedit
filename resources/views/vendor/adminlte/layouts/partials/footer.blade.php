<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <a href="https://dokedit.com"></a><b>DokEdit</b></a>. Edit dan Periksa Dokumen dengan Mudah
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; {{ date('Y') }} <a href="http://diffrntdigital.com" target="_blank">Diffrnt Digital</a>.</strong> All rights reserved.
</footer>