<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>
                </div>
            </div>
        @endif

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form" style="display: none;">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{ trans('adminlte_lang::message.search') }}..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">{{ trans('adminlte_lang::message.header') }}</li>
            <!-- Optionally, you can add icons to the links -->
            <li @if(preg_match('/dashboard/', Request::url())) class="active" @endif><a href="{{ url('home') }}"><i class='fa fa-dashboard'></i> <span>Dasbor</span></a></li>
            @if($logged_user->hasAccess(['documents.manage']) && $logged_user->is_editor() == FALSE)
            <li @if(Request::segment(1) == 'document') class="active" @else class="treeview" @endif>
                <a href="#"><i class="fa fa-files-o"></i> <span>Dokumen</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{ preg_match('/document\/manage/', Request::url()) ? 'active' : '' }}"><a href="{{ URL::Route('document.manage.get') }}"><i class="fa fa-file"></i> Dokumen Anda</a></li>
                    <li class="{{ preg_match('/document\/add/', Request::url()) ? 'active' : '' }}"><a href="{{ URL::Route('document.add.get') }}" title="Upload Dokumen Baru untuk Diedit"><i class="fa fa-plus"></i> Kirim Dokumen</a></a></li>
                </ul>
            </li>
            @elseif($logged_user->hasAccess(['documents.manage']) && $logged_user->is_editor() == TRUE)
            <li @if(Request::segment(1) == 'document') class="active" @else class="treeview" @endif>
                <a href="#"><i class="fa fa-files-o"></i> <span>Dokumen</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{ preg_match('/document\/manage\/available/', Request::url()) ? 'active' : '' }}">
                        <a href="{{ URL::Route('document.manage_for_editor.get', ['available']) }}"><i class="fa fa-file"></i> Dokumen Untuk Diedit</a>
                    </li>
                    <li class="{{ preg_match('/document\/manage\/in_progress/', Request::url()) ? 'active' : '' }}">
                        <a href="{{ URL::Route('document.manage_for_editor.get', ['in_progress']) }}"><i class="fa fa-pencil-square-o"></i> Dokumen Sedang Diedit</a>
                    </li>
                    <li class="{{ preg_match('/document\/manage\/completed/', Request::url()) ? 'active' : '' }}">
                        <a href="{{ URL::Route('document.manage_for_editor.get', ['completed']) }}"><i class="fa fa-check-square-o"></i> Dokumen Selesai Diedit</a>
                    </li>
                </ul>
            </li>
            @endif
            
            @if($logged_user->hasAccess(['users.manage']))
            <li @if(Request::segment(1) == 'admin') class="active" @else class="treeview" @endif>
                <a href="#"><i class='fa fa-users'></i> <span>Pengguna</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{ preg_match('/admin\/manage$/', Request::url()) ? 'active' : '' }}"><a href="{{ URL::Route('admin.manage.get') }}">Semua Pengguna</a></li>
                    <li class="{{ preg_match('/admin\/manage\/author/', Request::url()) ? 'active' : '' }}"><a href="{{ URL::Route('admin.manage.author.get') }}">Pemilik Dokumen</a></li>
                    <li class="{{ preg_match('/admin\/manage\/editor/', Request::url()) ? 'active' : '' }}"><a href="{{ URL::Route('admin.manage.editor.get') }}">Editor</a></li>
                    <!--<li><a href="#">Administrator</a></li>-->
                </ul>
            </li>
            @endif
            
            <li @if(preg_match('/profile\/my/', Request::url())) class="active" @endif><a href="{{ url('profile/my') }}"><i class='fa fa-user'></i> <span>{{ trans('adminlte_lang::message.my_profile') }}</span></a></li>
            
            @if($logged_user->hasAccess(['settings']))
            <li class="treeview">
                <a href="#"><i class='fa fa-gears'></i> <span>Pengaturan</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class='fa fa-money'></i> Tarif</a></li>
                </ul>
            </li>
            @endif
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
