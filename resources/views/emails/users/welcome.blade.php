<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        @if($user_type == 'author')
        {!! trans('users/register.welcome_email_content_author', ['name' => $name, 'app_name' => trans('app.app_name'), 'login_link' => $login_link, 'username' => $username ]) !!}
        @else
        {!! trans('users/register.welcome_email_content_editor', ['name' => $name, 'app_name' => trans('app.app_name'), 'login_link' => $login_link, 'username' => $username ]) !!}
        @endif
        
    </body>
</html>
