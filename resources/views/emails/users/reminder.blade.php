<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        {!! trans('users/forgot_password.reminder_email_content', ['name' => $name, 'app_name' => trans('app.app_name'), 'reminder_link' => $reminder_link]) !!}
    </body>
</html>
