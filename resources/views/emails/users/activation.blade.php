<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        @if($user_type == 'author')
        {!! trans('users/register.activation_email_content_author', ['name' => $name, 'app_name' => trans('app.app_name'), 'activation_link' => $activation_link ]) !!}
        @else
        {!! trans('users/register.activation_email_content_editor', ['name' => $name, 'app_name' => trans('app.app_name'), 'activation_link' => $activation_link ]) !!}
        @endif
    </body>
</html>
